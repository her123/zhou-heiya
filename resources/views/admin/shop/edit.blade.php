@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>门店信息修改</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript :history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{route('shop.update')}}" id="form">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$shopInfo->id}}">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">门店名称</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="shop_name" placeholder="请输入名称" value="{{$shopInfo->shop_name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">门店编码</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="shop_code" placeholder="请填写编码" value="{{$shopInfo->shop_code}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">门店区域</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="shop_area" placeholder="请填写所属区域" value="{{$shopInfo->shop_area}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">门店经度</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="shop_lng" placeholder="请填写门店经度" value="{{$shopInfo->shop_lng}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">门店纬度</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="shop_lat" placeholder="请填写门店纬度" value="{{$shopInfo->shop_lat}}">
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            beforeSubmit:function(){
                //提交前的验证

            },
            success:function(data){
                console.log(typeof(data));
                if(data.code === 10000){
                    layer.msg('门店信息修改'+data.message, {icon: 1,time:1500},function () {
                        window.location.href='{{route("shop.index")}}'
                    });
                }else{
                    layer.msg(data.message, {icon: 5,time:1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
