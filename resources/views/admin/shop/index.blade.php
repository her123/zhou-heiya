@include('admin.layouts._public_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>门店信息管理</h5>
                    <h5 style="float: right;margin-right: 30px;">
                        <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                            <i class="fa fa-refresh"></i> 刷新
                        </a>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="{{route('shop.add')}}">
                                <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增</button>
                            </a>
                            <a href="{{route('shop.import')}}" >
                                <button class="btn btn-primary" type="button" style="margin-left:20px;"><i class="fa fa-plus"></i>导入门店</button>
                            </a>

                            <form action="{{route('shop.index')}}" method="get">
                                <div class="input-group">
                                    {{ csrf_field() }}
                                    <input type="text" name="keywords" placeholder="请输入关键词"
                                           class="input-sm form-control" value="{{$request->keywords}}">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>门店编码</th>
                                <th>门店名称</th>
                                <th>门店区域</th>
                                <th>门店经度</th>
                                <th>门店纬度</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($shopList as $key=>$val)
                                <tr>
                                    <td>{{$val->id}}</td>
                                    <td>{{$val->shop_code}}</td>
                                    <td>{{$val->shop_name}}</td>
                                    <td>{{$val->shop_area}}</td>
                                    <td>{{$val->shop_lng}}</td>
                                    <td>
                                        {{$val->shop_lat}}
                                    </td>

                                    <td>

                                        <a href="{{route('shop.edit',['id'=>$val->id])}}" title="编辑">
                                            <i class="fa fa-edit"></i>&nbsp;编辑
                                        </a>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="javascript:;" onclick="deleteShop({{$val->id}})" title="删除">
                                            <i class="fa fa-trash-o"></i>&nbsp;删除
                                        </a>
                                        &nbsp;&nbsp;|
                                        <a href="{{route('account.add',['id'=>$val->id])}}" title="删除">
                                            <i class="fa fa-add"></i>&nbsp;门店账号
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$shopList->appends($search)->render()}} {{--->appends($search)--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteShop(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要删除该门店吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('shop.delete')}}", {id: id},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg('门店删除' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("shop.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }

    function PullBlack(id, type) {
        if (type == 1) {
            var title = '确定要禁用该银行卡信息吗？';
            var show = '禁用';
        } else {
            var title = '确定要解禁该银行卡信息吗？';
            var show = '解禁';
        }
        var lock = false;//默认未锁定
        layer.confirm(title, {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('bank.pull_back')}}", {id: id, type: type},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg(show + '银行卡信息' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("bank.index")}}'
                            });
                        } else {
                            layer.msg(show + '银行卡信息' + data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }
</script>
@include('admin.layouts._public_footer')
