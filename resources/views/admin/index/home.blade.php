@include('admin.layouts._public_header')
</head>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Welcome</h5>
                </div>
                <div class="ibox-content">
                    <h2>管理后台<br/></h2>
                    <p>欢迎使用 周黑鸭华南 管理后台</p>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>基本信息</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    运行环境
                                </div>
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">

                                            <tr>
                                                <th>PHP 版本</th>
                                                <td>{{PHP_VERSION}}</td>
                                            </tr>
                                            <tr>
                                                <th>PHP运行方式</th>
                                                <td>{{ php_sapi_name()}}</td>
                                            </tr>
                                            <tr>
                                                <th>MYSQL 版本</th>
                                                <td>{{mysqli_get_client_info()}}</td>
                                            </tr>
                                            <tr>
                                                <th>WEB 服务器</th>
                                                <td>{{$_SERVER['SERVER_SOFTWARE']}}</td>
                                            </tr>
                                            <tr>
                                                <th>服务器操作系统</th>
                                                <td>{{php_uname()}}</td>
                                            </tr>
                                            <tr>
                                                <th>laravel版本</th>
                                                <td>{{app()::VERSION}}</td>
                                            </tr>
                                            <tr>
                                                <th>opcache (建议开启)</th>
                                                @if(function_exists('opcache_get_configuration'))
                                                    <td>{{opcache_get_configuration()['directives']['opcache.enable'] ? '开启' : '关闭' }}</td>
                                                @else
                                                <td>未开启</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <th>脚本最大执行时间(s)</th>
                                                <td>{{get_cfg_var("max_execution_time")}}</td>
                                            </tr>
                                            <tr>
                                                <th>上传限制大小(M)</th>
                                                <td>{{get_cfg_var ("upload_max_filesize")}}</td>
                                            </tr>
                                            <tr>
                                                <th>当前时间</th>
                                                <td>{{date("Y-m-d H:i:s")}}</td>
                                            </tr>
                                            <tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div class="col-sm-6">--}}
                            {{--<div class="panel panel-primary">--}}
                                {{--<div class="panel-heading">更新日志</div>--}}
                                {{--<div class="panel-body">--}}
                                    {{--<div class="ibox-content timeline">--}}
                                        {{--<div class="timeline-item">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-xs-3 date">--}}
                                                    {{--<i class="fa fa-briefcase"></i> 6:00--}}
                                                    {{--<br>--}}
                                                    {{--<small class="text-navy">2018-11-16</small>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-xs-7 content no-top-border">--}}
                                                    {{--<p class="m-b-xs"><strong>后台框架搭建</strong></p>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-xs-3 date">--}}
                                                    {{--<i class="fa fa-briefcase"></i> 6:00--}}
                                                    {{--<br>--}}
                                                    {{--<small class="text-navy">2018-12-11</small>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-xs-7 content no-top-border">--}}
                                                    {{--<p class="m-b-xs"><strong>后台修复bug</strong></p>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-xs-3 date">--}}
                                                    {{--<i class="fa fa-briefcase"></i> 6:00--}}
                                                    {{--<br>--}}
                                                    {{--<small class="text-navy">2018-01-21</small>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-xs-7 content no-top-border">--}}
                                                    {{--<p class="m-b-xs"><strong>增加部分系统功能 & 修正 BUG</strong></p>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>
@include('admin.layouts._public_footer')