@section('title', '周黑鸭华南管理后台主页')
@section('keywords', '周黑鸭华南,管理后台,主页')
@section('description', '周黑鸭华南,管理后台,主页')
@include('admin.layouts._public_header')
</head>
<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
<div id="wrapper">
    @include('admin.layouts._menu')
    <!--右侧部分开始-->
    @include('admin.layouts._right_content')
    <!--右侧部分结束-->
    <!--右侧边栏开始-->
    @include('admin.layouts._right_sidebar')
    <!--右侧边栏结束-->
</div>
</body>
<!-- 第三方插件 -->
<script src="{{URL::asset('js/plugins/pace/pace.min.js')}}"></script>
@include('admin.layouts._public_footer')
