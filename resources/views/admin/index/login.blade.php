<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>华南周黑鸭后台管理 - 登录</title>
    <meta name="keywords" content="后台权限管理">
    <meta name="description" content="后台权限管理">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="{{URL::asset('css/bootstrap.min.css?v=3.3.6')}}" rel="stylesheet">
    <link href="{{URL::asset('css/font-awesome.css?v=4.4.0')}}" rel="stylesheet">

    <link href="{{URL::asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/style.css?v=4.1.0')}}" rel="stylesheet">
    <title>PithyAdmin通用后台权限管理</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <script>if(window.top !== window.self){ window.top.location = window.location;}</script>
    <style>
        .middle-box h1 {
            font-size: 70px !important;
        }
    </style>
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div class="login-panel">
        <div>
            <h1 class="logo-name">登陆界面</h1>
        </div>
        <h3></h3>
        <form class="m-t" role="form" id="form" method="post" action="{{route('doLogin')}}">
            <div class="form-group">
                <input type="name" class="form-control" name="name" placeholder="用户名" required>
            </div>
            {{ csrf_field() }}
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="密码" required>
            </div>
            {{--<button class="btn btn-primary" type="submit">登 录</button>--}}
            <button type="submit" class="btn btn-primary block full-width m-b login-submit">登 录</button>

        </form>
    </div>
</div>

<!-- 全局js -->
<script src="{{URL::asset('js/jquery.min.js?v=2.1.4')}}"></script>
<script src="{{URL::asset('js/bootstrap.min.js?v=3.3.6')}}"></script>
<script src="{{URL::asset('js/jquery.form.js')}}"></script>
<script src="{{URL::asset('layer/layer.js')}}"></script>
<script>
    var options={
        'dataType' : "JSON",
        beforeSubmit:function(){
            //提交前的验证
            layer.load(1, {
                    shade: [0.1,'#fff'] //0.1透明度的白色背景
                });
        },

        success:function(data){
            //成功之后的操作
            layer.closeAll();
            if(data.code == 10000){
                layer.msg('登陆'+ data.message, {icon: 1,time:1000},function () {
                    window.location.href="/admin";
                });
            }else{
                layer.msg(data.message, {icon: 5,time:1500});
            }
        }
    };
    $("#form").ajaxForm(options);

</script>
</body>

</html>
