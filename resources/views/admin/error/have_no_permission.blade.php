@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<style>
    .animated{
        margin-top: 10%;
    }
</style>
</head>
<body class="gray-bg">
    <div class="middle-box text-center animated fadeInDown">
        <p style="font-size: 50px">没有权限！</i>
        <h3 class="font-bold">
            <i id="wait">3</i>秒钟之后将自动跳转，
        </h3>
        <div class="error-desc">
           如果您的浏览器没有自动跳转，请点击<a href="javascript:;" onclick="goNow()">这里</a>
        </div>
    </div>
</body>
<script type="text/javascript">
    (function() {
        var wait = document.getElementById('wait');
        var interval = setInterval(function() {
            var time = --wait.innerHTML;
            if (time <= 0) {
                goNow();
                clearInterval(interval);
            }
            ;
        }, 1000);
    })();

    function goNow() {
        history.go(-1);
    }
</script>
@include('admin.layouts._public_footer')
