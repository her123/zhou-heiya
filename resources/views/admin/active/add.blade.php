@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>活动添加</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript:history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{route('active.add_active')}}" id="form">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动名称</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="active_name" placeholder="请输入活动名称">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">时间范围</label>
                            <div class="col-sm-6">
                                <input type="text" required name="start_time" class="col-sm-2 demo-input input-sm form-control select_no_border layer-date" autocomplete="off" placeholder="开启时间" onclick="laydate()" value="">
                                <span class="input-sm " style="float: left;">一</span>
                                <input style="float: left;" required type="text" name="end_time" class="col-sm-2 demo-input input-sm form-control select_no_border layer-date" autocomplete="off" placeholder="关闭时间" onclick="laydate()" value="">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动平台</label>

                            <div class="col-sm-10">
                                <textarea type="text" required class="form-control" name="platform" placeholder="请填写活动平台"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动介绍机制1</label>

                            <div class="col-sm-10">
                                <textarea type="text" style="height: 160px;" required class="form-control" name="mechanism" placeholder="请填写活动介绍机制1"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动介绍机制2</label>

                            <div class="col-sm-10">
                                <textarea type="text" style="height: 160px;" required class="form-control" name="describe" placeholder="请填写活动介绍机制2"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">一等奖奖励说明</label>

                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="level_reward1" placeholder="请填写一等奖奖励说明">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">二等奖奖励说明</label>

                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="level_reward2" placeholder="请填写二等奖奖励说明">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">三等奖奖励说明</label>

                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="level_reward3" placeholder="请填写三等奖奖励说明">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动备注</label>
                            <div class="col-sm-6">
                                <script id="active_introduction" name="remarks" type="text/plain" style="height:300px"></script>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">是否永久</label>

                            <div class="col-sm-10">
                                <select class="form-control m-b" name="is_infinite_time">
                                    <option value="1">是</option>
                                    <option value="0">否</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">状态</label>

                            <div class="col-sm-10">
                                <select class="form-control m-b" name="status">
                                    <option value="1">开启</option>
                                    <option value="0">结束</option>
                                </select>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var active_introduction = UE.getEditor('active_introduction');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            beforeSubmit:function(){
                //提交前的验证

            },
            success:function(data){
                console.log(typeof(data));
                if(data.code === 10000){
                    layer.msg('活动添加'+data.message, {icon: 1,time:1500},function () {
                        window.location.href='{{route("active.index")}}'
                    });
                }else{
                    layer.msg(data.message, {icon: 5,time:1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
