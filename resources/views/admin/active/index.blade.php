@include('admin.layouts._public_header')
<body class="gray-bg">
<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>活动管理</h5>
                <h5 style="float: right;margin-right: 30px;">
                    <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                        <i class="fa fa-refresh"></i> 刷新
                    </a>
                </h5>
            </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-3">
                    <a href="{{route('active.add')}}">
                        <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增</button>
                    </a>
                    <form action="{{route('active.index')}}" method="post">
                        <div class="input-group">
                            {{ csrf_field() }}
                            <input type="text" name="keywords" placeholder="请输入关键词" class="input-sm form-control" value="{{$request->keywords}}">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>活动名称</th>
                            <th>开启时间</th>
                            <th>结束时间</th>
                            <th>是否永久</th>
                            <th>活动状态</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse ($usersList as $probability)
                        <tr>
                            <td>{{$probability->active_name}}</td>
                            <td>{{$probability->start_time}}</td>
                            <td>{{$probability->end_time}}</td>
                            <td>@if ($probability->is_infinite_time == 1)是 @else 否@endif</td>
                            <td>@if ($probability->status == 1)开启 @else 关闭@endif</td>
                            <td>
                                <a href="{{route('active.edit',['id'=>$probability->id])}}" class="btn btn-primary">编辑</a>
                                <a href="{{route('active.users_lists',['id'=>$probability->id])}}" class="btn btn-primary">参与活动列表</a>
                                <a href="{{route('active.reward_lists',['id'=>$probability->id])}}" class="btn btn-primary">中奖列表</a>
                                <a href="{{route('active.active_qrcode',['id'=>$probability->id])}}" class="btn btn-primary">生存活动二维码</a>
{{--                                <a href="/code_lists/{{$probability->id}}" class="btn btn-primary">兑换码类型</a>--}}
{{--                                <a href="/code_send_add/{{$probability->id}}" class="btn btn-primary">发放兑换码</a>--}}
{{--                                <a href="/send_code_record/{{$probability->id}}" class="btn btn-primary">活动报表</a>--}}
                                <a href="javascript:return false;" onclick="reward({{$probability->id}});" title="开奖" class="btn btn-primary">开奖</a>
                                <a href="javascript:return false;" onclick="deleteUser({{$probability->id}});" title="删除" class="btn btn-primary">删除</a>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
                {{ $usersList->links() }}
            </div>

        </div>
    </div>
    </div>
</div>
</body>

</html>
<script>
    // 活动开奖
    function reward(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要开奖吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('active.reward')}}", {active_id: id},
                    function (data) {
                    console.log(data);
                        if (data.code === 10000) {
                            layer.msg('活动开奖' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("active.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }
        });
    }

    //删除该活动
    function deleteUser(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要删除该活动吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('active.delete')}}", {id: id},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg('活动删除' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("active.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }
</script>
