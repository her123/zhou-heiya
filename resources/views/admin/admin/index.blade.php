@include('admin.layouts._public_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>用户管理</h5>
                    <h5 style="float: right;margin-right: 30px;">
                        <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                            <i class="fa fa-refresh"></i> 刷新
                        </a>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="{{route('admin.add')}}">
                                <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增</button>
                            </a>
                            <form action="{{route('admin.index')}}" method="post">
                                <div class="input-group">
                                    {{ csrf_field() }}
                                    <input type="text" name="keywords" placeholder="请输入关键词" class="input-sm form-control" value="{{$request->keywords}}">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>用户名</th>
                                <th>昵称</th>
                                <th>性别</th>
                                <th>用户类型</th>
                                <th>邮箱</th>
                                <th>创建时间</th>
                                <th>最近登录时间</th>
                                <th>最近登录IP</th>
                                <th>用户状态</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usersList as $key=>$val)
                                <tr>
                                    <td>{{$val->id}}</td>
                                    <td>{{$val->name}}</td>
                                    <td>{{$val->nick_name}}</td>
                                    <td>{{$val->sex = 1 ? '男' : '女'}}</td>
                                    <td>{{$val->user_type = 1 ? '超级管理员' : '普通管理员'}}</td>
                                    <td>{{$val->email}}</td>
                                    <td>
                                       {{$val->created_at }}
                                    </td>
                                    <td>
                                        {{date("Y-m-d H:i:s",$val->last_login_at) }}
                                    </td>
                                    <td>
                                        {{$val->last_login_ip}}
                                    </td>
                                    <td>{{$val->status = 1 ? '正常' : '禁用'}}</td>

                                    <td>

                                        @if($val->id !== 1)
                                            <a href="{{route('admin.edit',['id'=>$val->id])}}" title="编辑">
                                                <i class="fa fa-edit"></i>&nbsp;编辑
                                            </a>
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="javascript:;" onclick="deleteUser({{$val->id}})" title="删除">
                                                <i class="fa fa-trash-o"></i>&nbsp;删除
                                            </a>
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            @if($val->user_status == 1)
                                                <a href="javascript:;" onclick="PullBlack('{{$val->id}}',1)" title="禁用">
                                                    <i class="fa fa-eye-slash"></i>&nbsp;禁用
                                                </a>
                                            @else
                                                <a href="javascript:;" onclick="PullBlack('{{$val->id}}',0)" title="启用">
                                                    <i class="fa fa-eye"></i>&nbsp;启用
                                                </a>
                                            @endif
                                        @else
                                            @if(in_array(1,Session::get('role_id')))
                                                <a href="{{route('admin.edit',['id'=>$val->id])}}" title="编辑">
                                                    <i class="fa fa-edit"></i>&nbsp;编辑
                                                </a>
                                                @else
                                                    <a href="javascript:return false;" onclick="prohibitOpt();" title="编辑" style="color: grey">
                                                        <i class="fa fa-edit"></i>&nbsp;编辑
                                                    </a>
                                                @endif
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="javascript:return false;" onclick="prohibitOpt();" title="删除" style="color: grey">
                                                <i class="fa fa-trash-o"></i>&nbsp;删除
                                            </a>
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="javascript:return false;" onclick="prohibitOpt();" title="禁用"  style="color: grey">
                                                <i class="fa fa-eye-slash"></i>&nbsp;禁用
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{$usersList->appends($search)->render()}} {{--->appends($search)--}}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteUser(id) {
        //询问框
        var lock=false;//默认未锁定
        layer.confirm('确定要删除该用户吗？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            if(!lock){
                lock = true;
                $.post("{{route('admin.delete')}}",{id:id},
                    function (data) {
                        if(data.code === 10000){
                            layer.msg('用户删除'+data.message, {icon: 1,time:1500},function () {
                                window.location.href='{{route("admin.index")}}'
                            });
                        }else{
                            layer.msg(data.message, {icon: 5,time:1500});
                        }
                    },'json')
            }

        });
    }
    function prohibitOpt() {
        layer.msg('超级管理员信息禁止操作', {icon: 4,time:1500});
    }
    function PullBlack(id,type) {
        if(type == 1){
            var title  = '确定要禁用该用户吗？';
            var show = '禁用';
        }else{
            var title = '确定要解禁该用户吗？';
            var show = '解禁';
        }
        var lock=false;//默认未锁定
        layer.confirm(title, {
            btn: ['确定','取消'] //按钮
        }, function(){
            if(!lock){
                lock = true;
                $.post("{{route('admin.pull_back')}}",{id:id,type:type},
                    function (data) {
                        if(data.code === 10000){
                            layer.msg(show+'用户'+data.message, {icon: 1,time:1500},function () {
                                window.location.href='{{route("admin.index")}}'
                            });
                        }else{
                            layer.msg(show+'用户'+data.message, {icon: 5,time:1500});
                        }
                    },'json')
            }

        });
    }
</script>
@include('admin.layouts._public_footer')
