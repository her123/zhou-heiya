@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>用户修改</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript :history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{route('admin.update')}}" id="form">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$userInfo->id}}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">用户名</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="name" placeholder="请输入用户名(4到16位字母,数字,下划线,减号)" value="{{ $userInfo->name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">用户类型</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="user_type">
                                    @foreach(config('params.userType') as $key=>$val)
                                        <option value="{{$key}}" @if($key == $userInfo->user_type) selected @endif >{{$val}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">昵称</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="nick_name" placeholder="请输入账户昵称" value="{{$userInfo->nick_name}}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">邮箱</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="email" placeholder="请填写邮箱" value="{{$userInfo->email}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">手机号</label>

                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="mobile" placeholder="请填写邮箱" value="{{$userInfo->mobile}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">性别</label>
                            <div class="col-sm-10">
                                <div class="radio i-checks">
                                    <input type="radio" value="1" name="sex" @if($userInfo->sex == 1) checked @endif > <i></i> 男
                                    <label>
                                        <input type="radio" value="2" name="sex" @if($userInfo->sex == 2) checked @endif> <i></i> 女
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">账户密码</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password" placeholder="请输入密码">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">确认密码</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password_confirmation" placeholder="请输入确认密码">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">用户角色</label>

                            <div class="col-sm-10">
                                @foreach($roles as $val)
                                    <label class="checkbox-inline i-checks">
                                        <input type="checkbox" name="role_id[]"
                                               @if(in_array($val->id,$rolesIds)) checked @endif
                                               @if(!in_array(1,Session::get('role_id')))
                                               @if($val->id == 1) disabled @endif
                                               @endif
                                               value="{{$val->id}}">
                                        {{$val->name}}
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            beforeSubmit:function(){
                //提交前的验证

            },
            success:function(data){
                console.log(typeof(data));
                if(data.code === 10000){
                    layer.msg('用户修改'+data.message, {icon: 1,time:1500},function () {
                        window.location.href='{{route("admin.index")}}'
                    });
                }else{
                    layer.msg(data.message, {icon: 5,time:1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
