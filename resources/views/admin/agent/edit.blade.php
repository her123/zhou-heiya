@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>优惠券修改</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript :history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="/update/agentrole" id="form">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$couponInfo->id}}">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">规则名称</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="name" placeholder="请输入优惠券名称" value="{{$couponInfo->name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">优惠券批次号</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="coupon_code" placeholder="请输入优惠券批次号" value="{{$couponInfo->coupon_code}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">核销获得奖励</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="rate_money"
                                       placeholder="请输入金额（如：0.00）" value="{{$couponInfo->rate_money}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动时间</label>
                            <div class="col-sm-6">
                                <input type="text" required name="start_time" class="col-sm-2 demo-input input-sm form-control select_no_border layer-date" autocomplete="off" placeholder="开启时间" onclick="laydate()" value="{{$couponInfo->start_time}}">
                                <span class="input-sm " style="float: left;">一</span>
                                <input style="float: left;" required type="text" name="end_time" class="col-sm-2 demo-input input-sm form-control select_no_border layer-date" autocomplete="off" placeholder="关闭时间" onclick="laydate()" value="{{$couponInfo->end_time}}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">导购员的条件</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="user_level">
                                    <option value="1" @if(1 == $couponInfo->user_level) selected @endif>鸭小圣</option>
                                    <option value="2" @if(2 == $couponInfo->user_level) selected @endif>鸭大仙</option>
                                    <option value="3" @if(3 == $couponInfo->user_level) selected @endif>飞天鸭</option>
                                    <option value="4" @if(4 == $couponInfo->user_level) selected @endif>一代宗鸭</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">状态</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="status">
                                    <option value="1" @if(1 == $couponInfo->status) selected @endif>进行中</option>
                                    <option value="2" @if(2 == $couponInfo->status) selected @endif>已结束</option>
                                </select>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            beforeSubmit:function(){
                //提交前的验证

            },
            success:function(data){
                console.log(typeof(data));
                if(data.code === 10000){
                    layer.msg('修改'+data.message, {icon: 1,time:1500},function () {
                        window.location.href='/agentrole/list'
                    });
                }else{
                    layer.msg(data.message, {icon: 5,time:1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
