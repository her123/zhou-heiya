@include('admin.layouts._public_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>规则列表</h5>
                    <h5 style="float: right;margin-right: 30px;">
                        <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                            <i class="fa fa-refresh"></i> 刷新
                        </a>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="/add/agentrole">
                                <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增</button>
                            </a>
                            <form action="/agentrole/list" method="get">
                                <div class="input-group">
                                    {{ csrf_field() }}
                                    <input type="text" name="keywords" placeholder="请输入关键词"
                                           class="input-sm form-control" value="{{$request->keywords}}">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>优惠券的批次号</th>
                                <th>规则名称</th>
                                <th>活动时间</th>
                                <th>满足的等级</th>
                                <th>核销获得奖励</th>
                                <th>状态</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usersList as $key=>$val)
                                <tr>
                                    <td>{{$val->id}}</td>
                                    <td>{{$val->coupon_code}}</td>
                                    <td>{{$val->name}}</td>
                                    <td>{{$val->start_time}}--{{$val->end_time}}</td>
                                    <td>{{$val->user_level}}</td>
                                    <td>{{$val->rate_money}}</td>
                                    <td>{{$val->status}}</td>
                                    <td>
                                        <a href="/edit/agentrole/{{$val->id}}" class="btn btn-primary">编辑</a>
                                        <a href="javascript:return false;" onclick="deleteUser({{$val->id}});" title="删除" class="btn btn-primary">删除</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$usersList->appends($search)->render()}}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteUser(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要删除该规则吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("/agentrole/delete", {id: id},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg('规则删除' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '/agentrole/delete'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }

</script>

@include('admin.layouts._public_footer')
