@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>积分商品添加</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript:history.back(1)" ;>
                            <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-backward"></i> 返回
                            </button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{route('mall.add_mall')}}" id="form">
                        {{csrf_field()}}

                        <div class="form-group">
                            <label class="col-sm-2 control-label">积分商品标题</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="title" placeholder="请输入积分商品标题">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">积分</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="point" placeholder="请输入积分">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">商品库存</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="sku_num" placeholder="请输入库存">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">金额</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="money"
                                       placeholder="请输入金额（如：0.00）">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">积分商品图片</label>
                            <div class="col-sm-10">
                                <input type="file" required name="file" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动备注</label>
                            <div class="col-sm-6">
                                <script id="active_introduction" name="detail" type="text/plain" style="height:300px"></script>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">是否热门</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="is_hot">
                                    <option value="1">否</option>
                                    <option value="2">是</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">是否最新</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="is_new">
                                    <option value="1">否</option>
                                    <option value="2">是</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">商品类型</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="type">
                                    <option value="1">普通商品</option>
                                    <option value="2">优惠券</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">优惠券批次号</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control" name="coupon_code" placeholder="商品类型是优惠券时请输入">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">状态</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="status">
                                    <option value="1">启用</option>
                                    <option value="0">禁用</option>
                                </select>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var active_introduction = UE.getEditor('active_introduction');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options = {
            'dataType': "JSON",
            beforeSubmit: function () {
                //提交前的验证

            },
            success: function (data) {
                console.log(typeof (data));
                if (data.code === 10000) {
                    layer.msg('操作' + data.message, {icon: 1, time: 1500}, function () {
                        window.location.href = '{{route("mall.index")}}'
                    });
                } else {
                    layer.msg(data.message, {icon: 5, time: 1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
