@include('admin.layouts._public_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>积分商品管理</h5>
                    <h5 style="float: right;margin-right: 30px;">
                        <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                            <i class="fa fa-refresh"></i> 刷新
                        </a>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="{{route('mall.add')}}">
                                <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增</button>
                            </a>
                            <form action="{{route('mall.index')}}" method="post">
                                <div class="input-group">
                                    {{ csrf_field() }}
                                    <input type="text" name="keywords" placeholder="请输入关键词"
                                           class="input-sm form-control" value="{{$request->keywords}}">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>积分商品标题</th>
                                <th>积分商品封面</th>
                                <th>所需积分</th>
                                <th>库存</th>
                                <th>所需金额</th>
                                <th>是否热门</th>
                                <th>是否最新</th>
                                <th>创建时间</th>
                                <th>状态</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mallList as $key=>$val)
                                <tr>
                                    <td>{{$val->id}}</td>
                                    <td>{{$val->title}}</td>
                                    <td><img style="width: 150px;height: 100px;" src="{{ img_url() . $val->images}}" alt="积分商品封面图"></td>
                                    <td>{{$val->point}}</td>
                                    <td>{{$val->sku_num}}</td>
                                    <td>{{$val->money??0.00}}</td>
                                    <td>{{$val->is_hot == 1 ? '否' : '是'}}</td>
                                    <td>{{$val->is_new == 1 ? '否' : '是'}}</td>
                                    <td>
                                        {{$val->created_at }}
                                    </td>
                                    <td>{{$val->status == 1 ? '正常' : '禁用'}}</td>
                                    <td>
                                        <a href="{{route('mall.edit',['id'=>$val->id])}}" title="编辑">
                                            <i class="fa fa-edit"></i>&nbsp;编辑
                                        </a>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="javascript:;" onclick="deleteUser({{$val->id}})" title="删除">
                                            <i class="fa fa-trash-o"></i>&nbsp;删除
                                        </a>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        @if($val->status == 1)
                                            <a href="javascript:;" onclick="PullBlack('{{$val->id}}',1)" title="禁用">
                                                <i class="fa fa-eye-slash"></i>&nbsp;禁用
                                            </a>
                                        @else
                                            <a href="javascript:;" onclick="PullBlack('{{$val->id}}',0)" title="启用">
                                                <i class="fa fa-eye"></i>&nbsp;启用
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$mallList->appends($search)->render()}} {{-- ->appends($search)--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteUser(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要删除该积分商品吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('mall.delete')}}", {id: id},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg('积分商品删除' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("mall.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }

    function PullBlack(id, type) {
        if (type == 1) {
            var title = '确定要禁用该积分商品吗？';
            var show = '禁用';
        } else {
            var title = '确定要解禁该积分商品吗？';
            var show = '解禁';
        }
        var lock = false;//默认未锁定
        layer.confirm(title, {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('mall.pull_back')}}", {id: id, type: type},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg(show + '积分商品' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("mall.index")}}'
                            });
                        } else {
                            layer.msg(show + '积分商品' + data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }
</script>
@include('admin.layouts._public_footer')
