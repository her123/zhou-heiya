<!--左侧导航开始-->
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="nav-close"><i class="fa fa-times-circle"></i>
    </div>
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{Session::get('user_name')}}</strong>
                            </span>
                            <span class="text-muted text-xs block">{{Session::get('nick_name')}}<b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a target="_blank" href="/admin/admin/edit_head_portrait?name=1&type=2">修改头像</a>
                        </li>
                        <li><a class="J_menuItem" href="profile.html">个人资料</a>
                        </li>
                        <li><a class="J_menuItem" href="contacts.html">联系我们</a>
                        </li>
                        <li><a class="J_menuItem" href="mailbox.html">信箱</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html">安全退出</a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element" style="font-size: 10px">周黑鸭华南
                </div>
            </li>

            @foreach($permissions as $permission)
                @if(!empty($permission['pid']))
                    <li>
                        <a href="#">
                            <i class="{{$permission['icon']}}"></i>
                            <span class="nav-label">{{$permission['name']}}</span><span class="fa arrow"></span>
                        </a>
                        @foreach($permission['pid'] as $permission_d1)
                            <ul class="nav nav-second-level">
                                @if(!empty($permission_d1['pid']))
                                    <li>
                                        <a href="#"> <i class="{{$permission_d1['icon']}}"></i>{{$permission_d1['name']}} <span class="fa arrow"></span></a>
                                        @foreach($permission_d1['pid'] as $permission_d2)
                                            <ul class="nav nav-third-level">
                                                <li><a class="J_menuItem" href="
{{action($permission_d2['module'].'\\'.$permission_d2['controller'].'@'.$permission_d2['action'])}}">{{--{{$permission_d2['routes']}}--}} <i class="{{$permission_d2['icon']}}"></i>{{$permission_d2['name']}}</a>
                                                </li>
                                            </ul>
                                        @endforeach
                                    </li>
                                @else
                                    <li>
                                        <a class="J_menuItem" href="{{action($permission_d1['module'].'\\'.$permission_d1['controller'].'@'.$permission_d1['action'])}}">{{--{{$permission_d1['routes']}}--}}
                                            <i class="{{$permission_d1['icon']}}"></i>
                                            {{$permission_d1['name']}}
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        @endforeach
                    </li>
                @else
                    <li>
                        <a class="J_menuItem" href="{{action($permission['module'].'\\'.$permission['controller'].'@'.$permission['action'])}}">
                            <i class="{{$permission['icon']}}"></i>
                            {{$permission['name']}}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</nav>
<!--左侧导航结束-->
