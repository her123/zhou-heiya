<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">

    <title>@yield('title','主页')</title>

    <meta name="keywords" content="@yield('keywords','通用管理后台')">
    <meta name="description" content="@yield('description','通用管理后台')">

    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

    <link rel="shortcut icon" href="favicon.ico">
    <link href="{{URL::asset('css/bootstrap.min.css?v=3.3.6')}}" rel="stylesheet">
    <link href="{{URL::asset('css/font-awesome.min.css?v=4.4.0')}}" rel="stylesheet">
    <link href="{{URL::asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/style.css?v=4.1.0')}}" rel="stylesheet">
    <!-- 全局js -->
    <script src="{{URL::asset('js/jquery.min.js?v=2.1.4')}}"></script>
    <script src="{{URL::asset('js/bootstrap.min.js?v=3.3.6')}}"></script>
    <script src="{{URL::asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{URL::asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{URL::asset('layer/layer.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/contabs.js')}}"></script>

    <!-- 自定义js -->
    <script src="{{URL::asset('js/hplus.js?v=4.1.0')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/jquery.form.min.js')}}"></script>
    <!-- 富文本 -->
    <script type="text/javascript" charset="utf-8" src="{{ URL::asset('editor/ueditor.config.js') }}"></script>
    <script type="text/javascript" charset="utf-8" src="{{ URL::asset('editor/ueditor.all.min.js') }}"> </script>
    <script type="text/javascript" charset="utf-8" src="{{ URL::asset('editor/lang/zh-cn/zh-cn.js') }}"></script>
    <script src="{{ URL::asset('js/plugins/layer/laydate/laydate.js') }}"></script>
    <style>
        .refresh-btn{
            display: initial;
        }
    </style>
    <script>
       function refresh() {
           window.location.reload();
       }
    </script>