@include('admin.layouts._public_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>积分商品管理</h5>
                    <h5 style="float: right;margin-right: 30px;">
                        <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                            <i class="fa fa-refresh"></i> 刷新
                        </a>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="{{route('article.add')}}">
                                <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增</button>
                            </a>
                            <form action="{{route('article.index')}}" method="post">
                                <div class="input-group">
                                    {{ csrf_field() }}
                                    <input type="text" name="keywords" placeholder="请输入关键词"
                                           class="input-sm form-control" value="{{$request->keywords}}">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>文案标题</th>
                                <th>创建时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($articleList as $key=>$val)
                                <tr>
                                    <td>{{$val->id}}</td>
                                    <td>{{$val->title}}</td>
                                    <td>
                                        {{$val->created_at }}
                                    </td>
                                    <td>
                                        <a href="{{route('article.edit',['id'=>$val->id])}}" title="编辑">
                                            <i class="fa fa-edit"></i>&nbsp;编辑
                                        </a>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="javascript:;" onclick="deleteUser({{$val->id}})" title="删除">
                                            <i class="fa fa-trash-o"></i>&nbsp;删除
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$articleList->appends($search)->render()}} {{-- ->appends($search)--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteUser(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要删除该文案吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('article.delete')}}", {id: id},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg('文案删除' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("article.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }
</script>
@include('admin.layouts._public_footer')
