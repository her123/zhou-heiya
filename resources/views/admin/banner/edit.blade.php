@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Banner修改</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript :history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{route('banner.update')}}" id="form">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$bannerInfo->id}}">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">标题</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="title" placeholder="请输入文案标题" value="{{$bannerInfo->title}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">跳转路径</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="jamp_url" placeholder="可以不填" value="{{$bannerInfo->jamp_url}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">目前图片</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="old_images" value="{{$bannerInfo->img_url}}">
                                <img src="{{img_url().$bannerInfo->img_url}}" style="width: 150px;height: 120px;">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">图片</label>
                            <div class="col-sm-10">
                                <input type="file" required name="img_url" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">来源</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="type">
                                    <option value="1"  @if(1 == $bannerInfo->type) selected @endif>积分商城</option>
                                    <option value="2"  @if(2 == $bannerInfo->type) selected @endif>门店打卡</option>
                                </select>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var active_introduction = UE.getEditor('active_introduction');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            beforeSubmit:function(){
                //提交前的验证

            },
            success:function(data){
                console.log(typeof(data));
                if(data.code === 10000){
                    layer.msg('操作'+data.message, {icon: 1,time:1500},function () {
                        window.location.href='{{route("banner.index")}}'
                    });
                }else{
                    layer.msg(data.message, {icon: 5,time:1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
