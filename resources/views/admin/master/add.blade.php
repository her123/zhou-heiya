@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>优惠券添加</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript:history.back(1)" ;>
                            <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-backward"></i> 返回
                            </button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="/add/costore" id="form">
                        {{csrf_field()}}

                        <div class="form-group">
                            <label class="col-sm-2 control-label">优惠券名称</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="name" placeholder="请输入优惠券名称">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">优惠券批次号</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="coupon_code" placeholder="请输入优惠券批次号">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">金额</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="coupon_money"
                                       placeholder="请输入金额（如：0.00）">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">库存</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="coupon_num" placeholder="请输入优惠券库存">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">状态</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="status">
                                    <option value="1">启用</option>
                                    <option value="0">禁用</option>
                                </select>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var active_introduction = UE.getEditor('active_introduction');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options = {
            'dataType': "JSON",
            beforeSubmit: function () {
                //提交前的验证

            },
            success: function (data) {
                console.log(typeof (data));
                if (data.code === 10000) {
                    layer.msg('操作' + data.message, {icon: 1, time: 1500}, function () {
                        window.location.href = '{{route("coupon.index")}}'
                    });
                } else {
                    layer.msg(data.message, {icon: 5, time: 1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
