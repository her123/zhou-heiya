@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>导购员审核</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript :history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="/update/master" id="form">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$couponInfo->id}}">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">手机号</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control disabled" name="mobile" value="{{$couponInfo->mobile}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">会员昵称</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control disabled"  value="{{$couponInfo->name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">会员等级</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control disabled"  value="{{$couponInfo->levelText}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">会员区域</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control disabled"  value="{{$couponInfo->area}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">状态</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="status">
                                    <option value="0" @if(0 == $couponInfo->status) selected @endif >申请中</option>
                                    <option value="1" @if(1 == $couponInfo->status) selected @endif >审核通过</option>
                                    <option value="2" @if(2 == $couponInfo->status) selected @endif >拒绝</option>
                                </select>
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            beforeSubmit:function(){
                //提交前的验证

            },
            success:function(data){
                console.log(typeof(data));
                if(data.code === 10000){
                    layer.msg('修改'+data.message, {icon: 1,time:1500},function () {
                        window.location.href='/master/list'
                    });
                }else{
                    layer.msg(data.message, {icon: 5,time:1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
