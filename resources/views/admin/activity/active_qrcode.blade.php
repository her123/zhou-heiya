@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>活动会员参加添加</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript:history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{route('active.create_qrcode')}}" id="form">
                        <div id="from-content-submit">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动门店</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="store" placeholder="请填写活动门店">
                            </div>
                        </div>

                        <input type="hidden" name="active_id" value="{{$active->id}}">
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                        </div>
                        <div id="qrcode-content" style="display: none;">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">二维码</label>
                                <div class="col-sm-10">
                                    <img id="qrcode-image-show" style="width: 220px;" title="门店活动二维码" src="#">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            beforeSubmit:function(){
                //提交前的验证

            },
            success:function(data){
                console.log(data);
                if(data.code === 10001){
                    layer.msg('生成二维码失败', {icon: 5,time:2500});
                }else{
                    $("#qrcode-image-show").attr("src",data.info);
                    $("#from-content-submit").hide();
                    $("#qrcode-content").show();
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
