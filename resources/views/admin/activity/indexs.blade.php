@include('admin.layouts._public_header')
<body class="gray-bg">
<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>活动管理</h5>
                <h5 style="float: right;margin-right: 30px;">
                    <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                        <i class="fa fa-refresh"></i> 刷新
                    </a>
                </h5>
            </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-3">
                    <a href="">
                        <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增</button>
                    </a>
                    <form action="{{route('log.index')}}" method="post">
                        <div class="input-group">
                            {{ csrf_field() }}
                            <input type="text" name="keywords" placeholder="请输入关键词" class="input-sm form-control" value="{{$request->keywords}}">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>手机号</th>
                            <th>积分增减</th>
                            <th>积分来源</th>
                            <th>时间</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse ($logList as $probability)
                        <tr>
                            <td>{{$probability->id}}</td>
                            <td>{{$probability->mobile}}</td>
                            <td>@if ($probability->type ==1)+ @else - @endif{{$probability->integral}}</td>
                            <td>@if ($probability->from_type ==1) 门店打卡
                                @elseif ($probability->from_type ==2) 步数星球
                                @else 积分商城
                                @endif()
                            </td>
                            <td>{{$probability->created_at}}</td>
                            <td>

                            </td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
                {{ $logList->links() }}
            </div>

        </div>
    </div>
    </div>
</div>
</body>

</html>
<script>
    // 活动开奖
    function reward(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要开奖吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('active.reward')}}", {active_id: id},
                    function (data) {
                    console.log(data);
                        if (data.code === 10000) {
                            layer.msg('活动开奖' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("active.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }
        });
    }

    //删除该活动
    function deleteUser(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要删除该活动吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('activity.deletes')}}", {id: id},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg('活动删除' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("activity.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }
</script>
