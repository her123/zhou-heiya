@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>活动修改</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript :history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{route('activity.update')}}" id="form">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$active->id}}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动名称</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="name" placeholder="请输入活动名称" value="{{$active->name}}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">时间范围</label>
                            <div class="col-sm-6">
                                <input type="text" required name="start_time" class="col-sm-2 demo-input input-sm form-control select_no_border layer-date" autocomplete="off" placeholder="开启时间" onclick="laydate()" value="{{$active->start_time}}">
                                <span class="input-sm " style="float: left;">一</span>
                                <input style="float: left;" required type="text" name="end_time" class="col-sm-2 demo-input input-sm form-control select_no_border layer-date" autocomplete="off" placeholder="关闭日期" onclick="laydate()" value="{{$active->end_time}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">目前图片</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="old_images" value="{{$active->banner_img}}">
                                <img src="{{config('app.img_url').$active->banner_img}}" style="width: 150px;height: 150px;">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">重新上传图片</label>
                            <div class="col-sm-10">
                                <input type="file" name="banner_img" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">满足的条件</label>

                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="condition" placeholder="请填写条件" value="{{$active->condition}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">奖励的积分</label>

                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="point" placeholder="请填写奖励的积分" value="{{$active->point}}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">优惠券批次号</label>

                            <div class="col-sm-10">
                                <input type="text"  class="form-control" name="coupon_code" placeholder="请填写优惠券批次号,和积分二选一" value="{{$active->coupon_code}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动内容</label>
                            <div class="col-sm-6">
                                <script id="active_introduction" name="content" type="text/plain" style="height:300px">{{$active->content}}</script>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">活动类型</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="type">
                                    <option value="1"  @if(1 == $active->type) selected @endif>积分兑换</option>
                                    <option value="2"  @if(2 == $active->type) selected @endif>领取优惠券</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">门店打卡的距离</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="distance" placeholder="门店打卡需要填写" value="{{$active->distance}}">
                            </div>
                        </div>


                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var active_introduction = UE.getEditor('active_introduction');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            beforeSubmit:function(){
                //提交前的验证

            },
            success:function(data){
                console.log(typeof(data));
                if(data.code === 10000){
                    layer.msg('活动修改'+data.message, {icon: 1,time:1500},function () {
                        window.location.href='{{route("activity.index")}}'
                    });
                }else{
                    layer.msg(data.message, {icon: 5,time:1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
