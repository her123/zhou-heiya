@include('admin.layouts._public_header')
<body class="gray-bg">
<div class="row">
    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>活动管理</h5>
                <h5 style="float: right;margin-right: 30px;">
                    <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                        <i class="fa fa-refresh"></i> 刷新
                    </a>
                </h5>
            </div>
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-3">
                    <a href="{{route('activity.add')}}">
                        <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增</button>
                    </a>
                    <form action="{{route('activity.index')}}" method="post">
                        <div class="input-group">
                            {{ csrf_field() }}
                            <input type="text" name="keywords" placeholder="请输入关键词" class="input-sm form-control" value="{{$request->keywords}}">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>活动名称</th>
                            <th>开启时间</th>
                            <th>结束时间</th>
                            <th>满足条件</th>
                            <th>奖励的积分</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse ($activityList as $probability)
                        <tr>
                            <td>{{$probability->name}}</td>
                            <td>{{$probability->start_time}}</td>
                            <td>{{$probability->end_time}}</td>
                            <td>{{$probability->condition}}</td>
                            <td>{{$probability->point}}</td>
                            <td>@if ($probability->status == 1)进行中 @else 已结束@endif</td>
                            <td>
                                <a href="{{route('activity.edit',['id'=>$probability->id])}}" class="btn btn-primary">编辑</a>
                                <a href="javascript:return false;" onclick="deleteUser({{$probability->id}});" title="删除" class="btn btn-primary">删除</a>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>
                {{ $activityList->links() }}
            </div>

        </div>
    </div>
    </div>
</div>
</body>

</html>
<script>
    // 活动开奖
    function reward(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要开奖吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('active.reward')}}", {active_id: id},
                    function (data) {
                    console.log(data);
                        if (data.code === 10000) {
                            layer.msg('活动开奖' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("active.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }
        });
    }

    //删除该活动
    function deleteUser(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要删除该活动吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('activity.deletes')}}", {id: id},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg('活动删除' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("activity.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }
</script>
