@include('admin.layouts._public_header')
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox-content">
        <div class="ibox-tools top_bottom">
            <a href="{{route('active.add_users')}}" class="btn btn-primary">添加</a>
            <a href="#" onClick="javascript :history.back(1)" class="btn btn-primary">返回</a>
        </div>
        <br/>
        <br/>
        <div class="table-responsive">
            <div class="row">
                <div class="col-sm-3">
                    <form action="{{route('active.users_lists')}}" method="post">
                        <div class="input-group">
                            {{ csrf_field() }}
                            <input type="text" name="keywords" placeholder="请输入关键词"
                                   class="input-sm form-control" value="{{$request->keywords}}">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>活动名称</th>
                    <th>会员手机号码</th>
                    <th>参与渠道来源</th>
                    <th>会员上传的图片</th>
                    <th>开启时间</th>
                    <th>结束时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($usersList as $probability)
                    <tr>
                        <td>{{$probability->active_name}}</td>
                        <td>{{$probability->mobile}}</td>
                        <td>@if ($probability->channel == 'H5')公众号链接 @elseif($probability->channel == 'store')
                                门店扫码 @else公众号链接@endif
                        </td>
                        <td>@if ($probability->active_images == '')该会员未上传图片@else<img style="width: 160px;height: 160px;"
                                                                                     src="{{ config('app.img_url') . $probability->active_images}}"
                                                                                     alt="七夕图片">@endif</td>
                        <td>{{$probability->start_time}}</td>
                        <td>{{$probability->end_time}}</td>
                        <td>
                            <a href="javascript:return false;"
                               onclick="setReward({{$probability->active_id}},1,{{$probability->mobile}});"
                               title="设置一级中奖"
                               class="btn btn-primary">设置一级中奖</a>
                            <a href="javascript:return false;"
                               onclick="setReward({{$probability->active_id}},2,{{$probability->mobile}});"
                               title="设置二级中奖"
                               class="btn btn-primary">设置二级中奖</a>
                            <a href="javascript:return false;"
                               onclick="setReward({{$probability->active_id}},3,{{$probability->mobile}});"
                               title="设置三级中奖"
                               class="btn btn-primary">设置三级中奖</a>
                            <a href="javascript:return false;" onclick="deleteUser({{$probability->m_id}});" title="删除"
                               class="btn btn-primary">删除</a>
                        </td>
                    </tr>
                @empty
                @endforelse
                </tbody>
            </table>
            {{ $usersList->links() }}
        </div>

    </div>
</div>
</body>

</html>
<script>
    function deleteUser(id) {
        //询问框
        var lock = false;//默认未锁定
        layer.confirm('确定要删除该会员参与活动吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('active.users_active_delete')}}", {id: id},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg('删除' + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("active.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }

    function setReward(active_id, level, mobile) {
        //询问框
        let lock = false;//默认未锁定
        let title = '';
        if (level === 1) {
            title = '一等奖';
        } else if (level === 2) {
            title = '二等奖';
        } else {
            title = '三等奖';
        }
        layer.confirm('确定要设置该会员为' + title + '吗？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            if (!lock) {
                lock = true;
                $.post("{{route('active.set_reward')}}", {active_id: active_id, level: level, mobile: mobile},
                    function (data) {
                        if (data.code === 10000) {
                            layer.msg('设置' + title + data.message, {icon: 1, time: 1500}, function () {
                                window.location.href = '{{route("active.index")}}'
                            });
                        } else {
                            layer.msg(data.message, {icon: 5, time: 1500});
                        }
                    }, 'json')
            }

        });
    }
</script>
