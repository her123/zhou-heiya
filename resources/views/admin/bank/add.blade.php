@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>银行卡信息添加</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript:history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{route('bank.add_bank')}}" id="form">
                        {{csrf_field()}}

                        <div class="form-group">
                            <label class="col-sm-2 control-label">所属用户</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="user_id">
                                    @foreach($users as $key=>$val)
                                        <option value="{{$val->id}}">用户ID：{{$val->id}}————用户名：{{$val->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">姓名</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="name" placeholder="请输入姓名">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">银行卡卡号</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="account" placeholder="请填写银行卡卡号">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">所属银行</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="bank" placeholder="请填写所属银行（如：中国银行）">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">办卡地区</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="area" placeholder="请填写办卡地区">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">预留手机号</label>
                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="mobile" placeholder="请填写预留手机号">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">用户状态</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="status">
                                    <option value="1">启用</option>
                                    <option value="0">禁用</option>
                                </select>
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            beforeSubmit:function(){
                //提交前的验证

            },
            success:function(data){
                console.log(typeof(data));
                if(data.code === 10000){
                    layer.msg('用户添加'+data.message, {icon: 1,time:1500},function () {
                        window.location.href='{{route("bank.index")}}'
                    });
                }else{
                    layer.msg(data.message, {icon: 5,time:1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
