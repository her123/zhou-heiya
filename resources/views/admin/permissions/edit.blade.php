@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>角色添加</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript :history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{route('permissions.save')}}" id="form">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">父级菜单</label>
                            <div class="col-sm-4">
                                <select class="form-control m-b" name="pid">
                                        <option value="">请选择父级菜单...</option>
                                        {!! $permissions !!}
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$permission->id}}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">菜单名称</label>

                            <div class="col-sm-4">
                                <input type="text" required class="form-control" name="name" value="{{$permission->name}}" placeholder="请输入菜单名称">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">菜单图标</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="icon" value="{{$permission->icon}}" placeholder="请输入菜单图标 格式： fa fa-home">
                                <div><a href="http://fontawesome.dashgame.com/" target="_blank">请跳转至图标库选择</a> 例如: fa fa-home</div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">模块</label>
                            <div class="col-sm-4">
                                <input type="text" required class="form-control" name="module" value="{{$permission->module}}" placeholder="请输入模块">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">控制器</label>
                            <div class="col-sm-4">
                                <input type="text" required class="form-control" value="{{$permission->controller}}" name="controller" placeholder="请输入控制器">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">方法</label>
                            <div class="col-sm-4">
                                <input type="text" required class="form-control" value="{{$permission->action}}" name="action" placeholder="请输入方法">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">路由</label>
                            <div class="col-sm-4">
                                <input type="text" required class="form-control" name="routes" value="{{$permission->routes}}" placeholder="请输入路由">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">路由别名</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="alias" value="{{$permission->alias}}" placeholder="请输入路由别名">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">是否显示</label>

                            <div class="col-sm-4">
                                <div class="radio i-checks">
                                    <input type="radio" value="1" name="is_show" @if($permission->is_show == 1) checked @endif > <i></i> 是
                                    <span>
                                        <input type="radio" value="2" name="is_show" @if($permission->is_show == 2) checked @endif> <i></i> 否
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            beforeSubmit:function(){
                //提交前的验证
            },
            success:function(data){
                console.log(typeof(data));
                if(data.code === 10000){
                    layer.msg('菜单修改'+data.message, {icon: 1,time:1500},function () {
                        window.location.href='{{route("permissions.index")}}'
                    });
                }else{
                    layer.msg('菜单修改'+data.message, {icon: 5,time:1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>

@include('admin.layouts._public_footer')
