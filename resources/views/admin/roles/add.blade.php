@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{URL::asset('css/ztree/bootstrapStyle/bootstrapStyle.css')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>角色添加</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript :history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="{{route('roles.add_roles')}}" id="form">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">角色名称</label>

                            <div class="col-sm-10">
                                <input type="text" required class="form-control" name="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">角色描述</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="descriptions"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">状态
                            </label>

                            <div class="col-sm-10">
                                <div class="radio i-checks">
                                    <input type="radio" value="1" name="status" checked> <i></i> 开启
                                    <label>
                                        <input type="radio" value="0" name="status"> <i></i> 禁用
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">保存内容</button>
                                <a href="#" onClick="javascript :history.back(1)">
                                    <button class="btn btn-white" type="button">取消</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/ztree/jquery.ztree.core.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/ztree/jquery.ztree.excheck.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/ztree/jquery.ztree.exedit.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        let options={
            'dataType' : "JSON",
            success:function(data){
                if(data.code === 10000){
                    layer.msg('角色添加'+data.message, {icon: 1,time:1500},function () {
                      window.location.href='{{route("admin.roles")}}'
                    });
                }else{
                    layer.msg(data.message, {icon: 5,time:1500});
                }
            }
        };
        $("#form").ajaxForm(options);
    });
</script>
@include('admin.layouts._public_footer')
