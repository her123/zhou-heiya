@include('admin.layouts._public_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>角色管理</h5>
                    <h5 style="float: right;margin-right: 30px;">
                        <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                            <i class="fa fa-refresh"></i> 刷新
                        </a>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="{{route('roles.add')}}">
                                <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增</button>
                            </a><br />
                            <form action="{{route('admin.roles')}}" method="post">
                                <div class="input-group">
                                    {{ csrf_field() }}
                                    <input type="text" name="name" placeholder="请输入关键词" class="input-sm form-control" value="{{$request->name}}">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                                </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>角色名称</th>
                                <th>角色描述</th>
                                <th>状态</th>
                                <th>创建时间</th>
                                <th>更新时间</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{$role->id}}</td>
                                    <td>
                                        <span class="pie">{{$role->name}}</span>
                                    </td>
                                    <td>
                                        <span class="pie">{{$role->descriptions}}</span>
                                    </td>
                                    <td>
                                        @if($role->status == 1)
                                            <i class="fa fa-check text-navy"></i>
                                        @else
                                            <i class="fa fa-close text-danger"></i>
                                        @endif
                                    </td>
                                    <td>{{$role->created_at}}</td>
                                    <td>{{$role->updated_at}}</td>
                                    <td>
                                        @if($role->id == 1)
                                            <a href="javascript:;" onclick="prohibitOpt();" title="权限设置" style="color: grey">
                                                <i class="fa fa-user-plus"></i>&nbsp;权限设置
                                            </a>
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="javascript:;" onclick="prohibitOpt();" title="编辑" style="color: grey">
                                                <i class="fa fa-edit"></i>&nbsp;编辑
                                            </a>
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="javascript:;" onclick="prohibitOpt();" title="删除" style="color: grey">
                                                <i class="fa fa-trash-o"></i>&nbsp;删除
                                            </a>
                                        @else
                                            <a href="{{route('roles.permissions',['role_id'=>$role->id])}}" title="权限设置">
                                                <i class="fa fa-user-plus"></i>&nbsp;权限设置
                                            </a>
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="{{route('roles.edit',['role_id'=>$role->id])}}" title="编辑">
                                                <i class="fa fa-edit"></i>&nbsp;编辑
                                            </a>
                                            &nbsp;&nbsp;|&nbsp;&nbsp;
                                            <a href="javascript:;" onclick="deleteRoles({{$role->id}})" title="删除">
                                                <i class="fa fa-trash-o"></i>&nbsp;删除
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                        {{$roles->appends($search)->render()}}
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function prohibitOpt() {
        layer.msg('超级管理员角色信息禁止操作', {icon: 4,time:1500});
    }
    function deleteRoles(id) {
        var lock=false;//默认未锁定
        layer.confirm('确定要删除该角色吗？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            if(!lock){
                lock = true;
                $.post("{{route('roles.delete')}}",{id:id},
                    function (data) {
                        if(data.code === 10000){
                            layer.msg('用户删除'+data.message, {icon: 1,time:1500},function () {
                                window.location.href='{{route("admin.roles")}}'
                            });
                        }else{
                            layer.msg(data.message, {icon: 5,time:1500});
                        }
                    },'json')
            }

        });
    }
</script>
@include('admin.layouts._public_footer');
