@include('admin.layouts._public_header')
<link href="{{URL::asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{URL::asset('css/ztree/bootstrapStyle/bootstrapStyle.css')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    .ztree{
        margin-left: 70px;
    }
</style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>角色权限</h5>
                    <div class="ibox-tools">
                        <a href="#" onClick="javascript :history.back(1)";>
                            <button type="button" class="btn btn-primary btn-xs">  <i class="fa fa-backward"></i> 返回</button>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <input type="hidden" name="role_id" value="{{$role_id}}">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">角色权限
                        </label>
                    </div>
                    <br>
                    <ul id="tree" class="ztree text-center"></ul>

                    <div class="hr-line-dashed"></div>


                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-1">
                            <button class="btn btn-primary" type="submit">保存内容</button>
                            <a href="#" onClick="javascript :history.back(1)">
                                <button class="btn btn-white" type="button">取消</button>
                            </a>
                        </div>
                    </div><br><br>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('js/plugins/iCheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/ztree/jquery.ztree.core.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/ztree/jquery.ztree.excheck.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/ztree/jquery.ztree.exedit.js')}}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    let setting = {
        view: {},
        check: {enable: true},
        async : {
            enable : true,
            url : '{{route("roles.getPermissions")}}',
            type: "post",
            otherParam:{
              'role_id':$('input[name=role_id]').val()
            },
        },
        data: {simpleData: {enable: true, pIdKey : "pid",}},
        callback:{
            onAsyncSuccess: zTreeOnAsyncSuccess,
            onCheck:onCheck
        }
    };
    let ids;
    $(document).ready(function(){
        $.fn.zTree.init($("#tree"), setting, null);
    });
    function zTreeOnAsyncSuccess(event, treeId, treeNode, msg) {
        ids = [];
        let treeObj=$.fn.zTree.getZTreeObj("tree");
        nodes = treeObj.getCheckedNodes(true);
        for(let i=0; i<nodes.length; i++){
            ids.push(nodes[i].id); //获取选中节点的值
        }
        console.log(ids)
    };
    function onCheck(e,treeId,treeNode){
        ids = [];
        let treeObj=$.fn.zTree.getZTreeObj("tree");
        nodes = treeObj.getCheckedNodes(true);
        for(let i=0; i<nodes.length; i++){
            ids.push(nodes[i].id); //获取选中节点的值
        }
        console.log(ids)
    }
    $(".btn-primary").click(function(){
        let roleId = $('input[name=role_id]').val();
        $.post("{{route('roles.permissions_add')}}", {role_id:roleId, permissions: ids}, function(data){
            if(data.code === 10000){
                layer.msg('角色权限添设置'+data.message, {icon: 1,time:1500},function () {
                    window.location.href='{{route("admin.roles")}}'
                });
            }else if(data.code === 10002) {
                layer.msg(data.message, {icon: 5,time:1500});
            }else{
                layer.msg('角色权限设置'+data.message, {icon: 5,time:1500});
            }
        },'json');
    })
</script>
@include('admin.layouts._public_footer')
