@include('admin.layouts._public_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>步数奖励说明列表</h5>
                    <h5 style="float: right;margin-right: 30px;">
                        <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                            <i class="fa fa-refresh"></i> 刷新
                        </a>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="{{route('step.add')}}">
                                <button class="btn btn-primary" type="button"><i class="fa fa-plus"></i> 新增</button>
                            </a>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>月份</th>
                            <th>奖励说明</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($usersList as $key=>$val)
                            <tr>
                                <td>{{$val->id}}</td>
                                <td>{{$val->month}}</td>
                                <td>{{$val->content}}</td>
                                <td>
                                    <a href="{{route('step.edit',['id'=>$val->id])}}" class="btn btn-primary">编辑</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$usersList->appends($search)->render()}}
                </div>
            </div>
        </div>
    </div>

</div>
</body>
@include('admin.layouts._public_footer')
