@include('admin.layouts._public_header')
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>会员步数列表</h5>
                    <h5 style="float: right;margin-right: 30px;">
                        <a class="btn btn-white btn-bitbucket refresh-btn" onclick="refresh();">
                            <i class="fa fa-refresh"></i> 刷新
                        </a>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <form action="{{route('users.index')}}" method="post">
                                <div class="input-group">
                                    {{ csrf_field() }}
                                    <input type="text" name="keywords" placeholder="请输入关键词"
                                           class="input-sm form-control" value="{{$request->keywords}}">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> 搜索</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>会员手机号码</th>
                                <th>步数日期</th>
                                <th>步数</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usersList as $key=>$val)
                                <tr>
                                    <td>{{$val->id}}</td>
                                    <td>{{$val->mobile}}</td>
                                    <td>{{$val->step}}</td>
                                    <td>{{$val->date}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$usersList->appends($search)->render()}}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@include('admin.layouts._public_footer')
