<?php

namespace App\Imports;

use App\Models\Shop;
use Maatwebsite\Excel\Concerns\ToModel;

class AdminsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Shop([
            //数据
            'shop_code' => $row[0],
            'shop_name' => $row[1],
            'shop_area' => $row[2],
            'shop_lng'  => $row[3],
            'shop_lat'  => $row[4],
         ]);
    }
}
