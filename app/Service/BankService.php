<?php
namespace App\Service;

use App\Models\Admin\Bank;
use App\Models\Admin\Roles;

class BankService
{
    protected static $bank;

    public function __construct(Bank $bank)
    {
        self::$bank= $bank;
    }

    /**
     * 用户添加
     * @param $data
     * @return bool
     * @author:
     * @date: 2019/5/20 15:30
     */
    public function store($data)
    {
        return self::$bank::query()->insertGetId($data);
    }


    /**
     * 获取用户信息详情
     * @param $id
     * @return Bank[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/20 17:55
     */
    public function get($id)
    {
        return self::$bank::find($id);
    }
    /**
     * 用户信息修改
     * @param $data
     * @return mixed
     * @author:
     * @date: 2019/5/20 19:36
     */
    public function update($data)
    {
        $user = self::$bank::find($data['id']);
        unset($data['id']);
        foreach ($data as $key=>$val){
            $user->$key = $val;
        }
        return $user->save();
    }

    /**
     * 删除用户
     * @param $id
     * @author:
     * @date: 2019/5/20 20:22
     */
    public function delete($id)
    {
        return self::$bank::where('id',$id)->delete();
    }

    /**
     * 用户拉黑
     * @param $id
     * @return bool
     * @author:
     * @date: 2019/5/22 15:32
     */
    public function pullBlack($id,$userStatus)
    {
        $user = self::$bank::find($id);
        $user->status = $userStatus;
        return $user->save();
    }


    /**
     * 获取所有银行卡信息
     * @return Roles[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/24 16:44
     */
    public function allBanks()
    {
        return self::$bank::get();
    }


    /**
     * 格式化菜单列表数据
     * @param $category
     * @param int $pid
     * @param int $level
     * @return array
     * @author:
     * @date: 2019/5/29 16:59
     */
    public static function categoryTree($category, $pid = 0, $level = 0) {
        static $res = array();
        foreach($category as $v) {
            if($v['pid'] == $pid) {
                $v['level'] = $level;
                $res[] = $v;
                self::categoryTree($category,$v['id'],$level+1);
            }
        }
        return $res;
    }


    /**
     * 下拉框显示格式化菜单数据  缩进
     * @param $list
     * @param int $id
     * @return string
     * @author:
     * @date: 2019/5/29 16:59
     */
    public static function showSelect($list, $id = 0){
        $str = '';
        foreach($list as $row) {
            $select = '';
            if($id==$row['id']){
                $select = "selected";
            }
            $str .= '<option value="' . $row['id'] . '"' . $select . '>' .
                    $row['name'] .
                '</option>';
        }
        return $str;
    }


    /**
     * 获取菜单列表
     * @param $fields
     * @param array $where
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/23 11:43
     */
    public function getBankList($fields,$where = [])
    {
        return self::$bank::query()->select($fields)->where($where)->get();
    }
}
