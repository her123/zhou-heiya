<?php
namespace App\Service;

use App\Models\Master;

class MasterService
{
    protected static $agent;

    public function __construct(Master $mall)
    {
        self::$agent= $mall;
    }

    /**
     * 用户添加
     * @param $data
     * @return bool
     * @author:
     * @date: 2019/5/20 15:30
     */
    public function store($data)
    {
        return self::$agent::query()->insertGetId($data);
    }


    /**
     * 获取用户信息详情
     * @param $id
     * @return agent[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/20 17:55
     */
    public function get($id)
    {
        return self::$agent::find($id);
    }
    /**
     * 用户信息修改
     * @param $data
     * @return mixed
     * @author:
     * @date: 2019/5/20 19:36
     */
    public function update($data)
    {
        $user = self::$agent::find($data['id']);
        unset($data['id']);
        foreach ($data as $key=>$val){
            $user->$key = $val;
        }
        return $user->save();
    }

    /**
     * 用户信息修改
     * @param $data
     * @return mixed
     * @author:
     * @date: 2019/5/20 19:36
     */
    public function updates($where,$data=[])
    {
        $user = self::$agent::where($where);
        return $user->save($data);
    }

    /**
     * 删除用户
     * @param $id
     * @return
     * @author:
     * @date: 2019/5/20 20:22
     */
    public function delete($id)
    {
        return self::$agent::where('id',$id)->delete();
    }

    /**
     * 用户拉黑
     * @param $id
     * @param $status
     * @return bool
     * @author:
     * @date: 2019/5/22 15:32
     */
    public function pullBlack($id,$status)
    {
        $user = self::$agent::find($id);
        $user->status = $status;
        return $user->save();
    }

    /**
     * 获取推销上级信息
     * @param array $where
     * @return
     */
    public function getMaster($where=[]){
        return self::$agent::where($where)->first();
    }


    /**
     * 获取所有银行卡信息
     * @return Agent[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/24 16:44
     */
    public function allAgent()
    {
        return self::$agent::get();
    }

}
