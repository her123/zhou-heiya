<?php
/**
 * Created by PhpStorm.
 * User: yunli
 * Date: 2019/5/5
 * Time: 下午6:28
 */

namespace App\Service;


use Illuminate\Support\Facades\Redis;

class CacheService
{

    const RETRIEVE_PWD_TOKEN = 'zhouheiya_';

    public static function getTest()
    {
        return Redis::get('li');
    }

    public static function setTest()
    {
        Redis::setex('li', 300, 'yun');
    }


    /**
     * getUuidEncrypt
     * @param string $key
     * @return mixed
     * @user yun.li
     * @time 2019/1/16 下午5:02
     */
    public static function getKey(string $key)
    {
        return Redis::get(self::RETRIEVE_PWD_TOKEN.$key);
    }

    /**
     * setUuidEncrypt
     * @param string $key
     * @param string $value
     * @user yun.li
     * @time 2019/1/16 下午5:02
     */
    public static function setKey(string $key, string $value)
    {
        Redis::setex(self::RETRIEVE_PWD_TOKEN.$key, $value);
    }

    /**
     * setUuidEncrypt
     * @param string $key
     * @param string $value
     * @param int $timestamp
     * @user yun.li
     * @time 2019/1/16 下午5:02
     */
    public static function setExKey($key, $value,$timestamp = 3600)
    {
        Redis::setex(self::RETRIEVE_PWD_TOKEN.$key, $timestamp, $value);
    }

    /**
     * delRetrievePwdToken
     * @param string $key
     * @user yun.li
     * @time 2019/5/19 下午4:35
     */
    public static function delRetrievePwdToken(string $key)
    {
        Redis::del(self::RETRIEVE_PWD_TOKEN.$key);
    }

    /**
     * @param $list
     * @param $value
     */
    public static function setList($list, $value){
        return Redis::lpush(self::RETRIEVE_PWD_TOKEN.$list,$value);
    }

    public static function getList($list){
        return Redis::lrange(self::RETRIEVE_PWD_TOKEN.$list,0,-1);
    }

//    public static function randomList($list){
//        return Redis::Srandmember(self::RETRIEVE_PWD_TOKEN.$list,4);
//    }

    public static function delList($list,$count,$value){
        return Redis::lrem(self::RETRIEVE_PWD_TOKEN.$list,$count,$value);
//        LREM key count value
    }
}
