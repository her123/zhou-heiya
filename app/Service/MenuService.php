<?php


namespace App\Service;


class MenuService
{
    /**
     * 格式化菜单（树结构）
     * @param $data
     * @param int $pid
     * @param int $deep
     * @return array
     * @author: zhangfayuan
     * @date: 2019/7/13 13:57
     */
    public static function  getMenu($data,$pid = 0,$deep=0)
    {
        $tree=[];
        foreach ($data as $row) {
            if($row['pid']==$pid){
                $row['deep']=$deep;
                $row['pid']=self::getMenu($data,$row['id'],$deep+1);
                $tree[]=$row;
            }
        }
        return $tree;

    }

    /**
     * 格式化菜单列表数据
     * @param $category
     * @param int $pid
     * @param int $level
     * @return array
     * @author:
     * @date: 2019/5/29 16:59
     */
    public static function categoryTree($category, $pid = 0, $level = 0) {
        static $res = array();
        foreach($category as $v) {
            if($v['pid'] == $pid) {
                $v['level'] = $level;
                $res[] = $v;
                self::categoryTree($category,$v['id'],$level+1);
            }
        }
        return $res;
    }

    /**
     * 下拉框显示格式化菜单数据  缩进
     * @param $list
     * @param int $id
     * @return string
     * @author:
     * @date: 2019/5/29 16:59
     */
    public static function showSelect($list, $id = 0){
        $str = '';
        foreach($list as $row) {
            $select = '';
            if($id==$row['id']){
                $select = "selected";
            }
            $str .= '<option value="' . $row['id'] . '"' . $select . '>' .
                str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $row['level']) . $row['name'] .
                '</option>';
        }
        return $str;
    }
}
