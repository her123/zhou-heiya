<?php
namespace App\Service;

use App\Models\Admin\Roles;
use App\Models\Admin\Users;

class UsersService
{
    protected static $users;

    public function __construct(Users $users)
    {
        self::$users= $users;
    }

    /**
     * 用户添加
     * @param $data
     * @return bool
     * @author:
     * @date: 2019/5/20 15:30
     */
    public function store($data)
    {
        return self::$users::query()->insertGetId($data);
    }

    /**
     * 获取用户信息详情
     * @param $id
     * @return Users[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/20 17:55
     */
    public function get($id)
    {
        return self::$users::find($id);
    }
    /**
     * 用户信息修改
     * @param $data
     * @return mixed
     * @author:
     * @date: 2019/5/20 19:36
     */
    public function update($data)
    {
        $user = self::$users::find($data['id']);
        unset($data['id']);
        foreach ($data as $key=>$val){
            $user->$key = $val;
        }
        return $user->save();
    }

    /**
     * 删除用户
     * @param $id
     * @author:
     * @date: 2019/5/20 20:22
     */
    public function delete($id)
    {
        return self::$users::where('id',$id)->delete();
    }


    /**
     * 删除用户
     * @param $mobile
     * @return
     * @author:
     * @date: 2019/5/20 20:22
     */
    public function getUsers($mobile)
    {
        return self::$users::where('mobile',$mobile)->first();
    }

    /**
     * 获取菜单列表
     * @param $fields
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/23 11:43
     */
    public function getUserList($fields,$where = [])
    {
        return self::$users::query()->select($fields)->where($where)->get();
    }

    /**
     * 用户拉黑
     * @param $id
     * @return bool
     * @author:
     * @date: 2019/5/22 15:32
     */
    public function pullBlack($id,$userStatus)
    {
        $user = self::$users::find($id);
        $user->status = $userStatus;
        return $user->save();
    }

    /**
     * 获取所有用户信息
     * @return Roles[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/24 16:44
     */
    public function allUsers()
    {
        return self::$users::get();
    }
}
