<?php
namespace App\Service;


use App\Models\Admin\UsersActiveAssociation;

class UsersActiveAssociationService
{
    protected static $users;

    public function __construct(UsersActiveAssociation $users)
    {
        self::$users= $users;
    }

    /**
     * 用户添加
     * @param $data
     * @return bool
     * @author:
     * @date: 2019/5/20 15:30
     */
    public function store($data)
    {
        return self::$users::query()->insertGetId($data);
    }

    /**
     * 获取用户信息详情
     * @param $id
     * @return UsersActive[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/20 17:55
     */
    public function get($id)
    {
        return self::$users::find($id);
    }
    /**
     * 用户信息修改
     * @param $data
     * @return mixed
     * @author:
     * @date: 2019/5/20 19:36
     */
    public function update($data)
    {
        $user = self::$users::find($data['id']);
        unset($data['id']);
        foreach ($data as $key=>$val){
            $user->$key = $val;
        }
        return $user->save();
    }

    /**
     * 删除用户
     * @param $id
     * @return
     * @author:
     * @date: 2019/5/20 20:22
     */
    public function delete($id)
    {
        return self::$users::where('id',$id)->delete();
    }


    /**
     *
     * @return
     * @author:
     * @date: 2019/5/20 20:22
     */
    public function getActive()
    {
        return self::$users::where('status',1)->first();
    }

    public function getActiveInfo($where = [])
    {
        return self::$users::where($where)->first();
    }

    /**
     * 获取菜单列表
     * @param $fields
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/23 11:43
     */
    public function getUserList($fields,$where = [])
    {
        return self::$users::query()->select($fields)->where($where)->get();
    }


    /**
     * @param array $where
     * @return
     */
    public function getCount($where = [])
    {
        return self::$users::where($where)->count();
    }

    /**
     * 用户拉黑
     * @param $id
     * @param $userStatus
     * @return bool
     * @author:
     * @date: 2019/5/22 15:32
     */
    public function pullBlack($id,$userStatus)
    {
        $user = self::$users::find($id);
        $user->status = $userStatus;
        return $user->save();
    }

    /**
     * 获取所有用户信息
     * @return Roles[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/24 16:44
     */
    public function allUsers()
    {
        return self::$users::get();
    }
}
