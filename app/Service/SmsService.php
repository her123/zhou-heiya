<?php

namespace App\Service;

class SmsService
{

    /**
     * @param $http
     * @param $mobile
     * @param $msg
     * @param string $needstatus
     * @return string
     */
    public function sendSMS($http, $mobile, $msg, $needstatus = 'true')
    {
        //创蓝接口参数
        $postArr = array(
            'account' => config('app.chuanglan_sdk.app_key'),
            'password' => config('app.chuanglan_sdk.app_secret'),
            'msg' => urlencode($msg),
            'phone' => $mobile,
            'report' => $needstatus,
        );

        try {
            $result = $http->HttpRequest(config('app.chuanglan_sdk.sms_url'), 'POST', $postArr);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $result;
    }

    /**
     * 发送变量短信
     *
     * @param $http
     * @param string $msg 短信内容
     * @param string $params 最多不能超过1000个参数组
     * @return string
     */
    public function sendVariableSMS($http, $msg, $params)
    {

        //创蓝接口参数
        $postArr = array(
            'account' => config('app.chuanglan_sdk.app_key'),
            'password' => config('app.chuanglan_sdk.app_secret'),
            'msg' => $msg,
            'params' => $params,
            'report' => 'true'
        );
        try {
            $result = $http->HttpRequest(config('app.chuanglan_sdk.variable_url'), 'POST', $postArr);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $result;
    }


    /**
     * 查询额度
     *
     *  查询地址
     * @param $http
     * @return array|bool|false|float|int|object|string|null
     */
    public function queryBalance($http)
    {

        //查询参数
        $postArr = array(
            'account' => config('app.chuanglan_sdk.app_key'),
            'password' => config('app.chuanglan_sdk.app_secret'),
        );
        try {
            $result = $http->HttpRequest(config('app.chuanglan_sdk.balance_url'), 'POST', $postArr);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $result;
    }
}
