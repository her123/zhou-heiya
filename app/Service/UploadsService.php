<?php
namespace App\Service;
use Illuminate\Support\Facades\Storage;

class UploadsService
{
    /**
     * 上传图片
     * @param $file
     * @return false|string
     */
    public function uploads_img($file)
    {
        if ($file->isValid()) {
            // 判断上传图片的格式
            $type = $file->extension();
            if(!in_array($type,['png','jpg','jpeg','gif','GIF','JPG','PNG','JPEG'])){
                return tips('error','上传格式不支持');
            }

            //判断上传图片的大小
            $fileSize = $file->getSize();
            if($fileSize > 1024*1024*2){
                return tips('error','图片过大,不得超过5MB');
            }

            try {
                $path = $file->store('images/'.date('Y-m-d'));
                if (!$path) {
                    throw new \Exception('上传图片异常');
                }
            } catch (\Exception $e) {
                return tips('error', $e->getMessage());
            }
            return $path;
        }
    }

    /**
     * 删除图片
     * @param $img
     * @return bool
     */
    public function delete_img($img){
        return Storage::delete($img);
    }
}
