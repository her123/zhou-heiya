<?php
namespace App\Service;

use App\Models\Admin\Step;

class StepService
{
    protected static $step;

    public function __construct(Step $mall)
    {
        self::$step= $mall;
    }

    /**
     * 用户添加
     * @param $data
     * @return bool
     * @author:
     * @date: 2019/5/20 15:30
     */
    public function store($data)
    {
        return self::$step::query()->insert($data);
    }


    /**
     * 获取用户信息详情
     * @param $id
     * @return Step[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/20 17:55
     */
    public function get($id)
    {
        return self::$step::find($id);
    }
    /**
     * 用户信息修改
     * @param $data
     * @return mixed
     * @author:
     * @date: 2019/5/20 19:36
     */
    public function update($data)
    {
        $user = self::$step::find($data['id']);
        unset($data['id']);
        foreach ($data as $key=>$val){
            $user->$key = $val;
        }
        return $user->save();
    }

    /**
     * 删除用户
     * @param $id
     * @return
     * @author:
     * @date: 2019/5/20 20:22
     */
    public function delete($id)
    {
        return self::$step::where('id',$id)->delete();
    }

    /**
     * 用户拉黑
     * @param $id
     * @param $status
     * @return bool
     * @author:
     * @date: 2019/5/22 15:32
     */
    public function pullBlack($id,$status)
    {
        $user = self::$step::find($id);
        $user->status = $status;
        return $user->save();
    }


    public function getStep($where=[]){
        return self::where($where)->first();
    }


    /**
     * 获取所有银行卡信息
     * @return Step[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/24 16:44
     */
    public function allStep()
    {
        return self::$step::get();
    }

}
