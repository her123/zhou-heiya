<?php
namespace App\Service;

use App\Models\Admin\PointsMall;

class PointsMallService
{
    protected static $mall;

    public function __construct(PointsMall $mall)
    {
        self::$mall= $mall;
    }

    /**
     * 用户添加
     * @param $data
     * @return bool
     * @author:
     * @date: 2019/5/20 15:30
     */
    public function store($data)
    {
        return self::$mall::query()->insertGetId($data);
    }


    /**
     * 获取用户信息详情
     * @param $id
     * @return PointsMall[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/20 17:55
     */
    public function get($id)
    {
        return self::$mall::find($id);
    }
    /**
     * 用户信息修改
     * @param $data
     * @return mixed
     * @author:
     * @date: 2019/5/20 19:36
     */
    public function update($data)
    {
        $user = self::$mall::find($data['id']);
        unset($data['id']);
        foreach ($data as $key=>$val){
            $user->$key = $val;
        }
        return $user->save();
    }

    /**
     * 删除用户
     * @param $id
     * @author:
     * @date: 2019/5/20 20:22
     */
    public function delete($id)
    {
        return self::$mall::where('id',$id)->delete();
    }

    /**
     * 用户拉黑
     * @param $id
     * @return bool
     * @author:
     * @date: 2019/5/22 15:32
     */
    public function pullBlack($id,$userStatus)
    {
        $user = self::$mall::find($id);
        $user->status = $userStatus;
        return $user->save();
    }


    /**
     * 获取所有银行卡信息
     * @return PointsMall[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/24 16:44
     */
    public function allMall()
    {
        return self::$mall::get();
    }

}
