<?php
namespace App\Service;

use App\Models\Shop;

use Illuminate\Support\Facades\DB;

class ShopClockService
{
    protected static $clock;

    public function __construct(Shop $mall)
    {
        self::$clock= $mall;
    }

    /**
     *
     * @param $lng
     * @param $lat
     * @param $distance
     * @author Bryant
     * @date 2020-09-02 16:58
     *
     * 获取经纬度范围内的门店
     */
    public function getList($lng,$lat,$distance)
    {
        $list = self::$clock::query()->selectRaw( "round((
                            6371392.89 * acos (
                            cos ( radians({$lat}) )
                             * cos( radians(lat ) )
                             * cos( radians(lng) - radians({$lng}) )
                             + sin ( radians({$lat}) )
                             * sin( radians( lat ) )
                             )), 0) AS distance")
            ->havingRaw('distance <= ?',[$distance])
            ->paginate();

        return $list;
    }

    /**
     * 删除用户
     * @param $id
     * @return
     * @author:
     * @date: 2019/5/20 20:22
     */
    public function delete($id)
    {
        return self::$clock::where('id',$id)->delete();
    }
}
