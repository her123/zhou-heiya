<?php
/**
 * Created by PhpStorm.
 * User: yunli
 * Date: 2019/4/30
 * Time: 上午11:58
 */

use Illuminate\Http\JsonResponse;

/**
 * 显示提示
 * @param $status
 * @param $code
 * @param array $data
 * @return JsonResponse
 */

function tips($status,$code, $data = [])
{
    return response()->json([
        'status' => $status,
        'code' => $code,
        'data' => $data,
    ]);
}


/**
 * 验证手机号
 * @param $mobile
 * @return false|string
 */
function checkMobile($mobile)
{
    return preg_match('/^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\d{8}$/i', $mobile);
}


/**
 * 判断是不是json数据
 * true 是， false 不是
 * @param $str
 * @return bool
 */
function is_json($str){
    $flag = is_null(json_decode($str));
    return $flag===true?false:true;
}

function get_month_name($month = ''){
    $month_array =  [
        1 => '一月',
        2 => '二月',
        3 => '三月',
        4 => '四月',
        5 => '五月',
        6 => '六月',
        7 => '七月',
        8 => '八月',
        9 => '九月',
        10 => '十月',
        11 => '十一月',
        12 => '一二月',
    ];
    if($month){
        return $month_array[$month];
    }
    return $month_array;
}

/**
 *
 * @author Bryant
 * @date 2020-10-20 17:29
 *
 * 图片根路径
 */
function img_url(){
    return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME']."/storage/";
}


/**
 *
 * @author Bryant
 * @date 2020-10-23 9:42
 *
 * 网站根目录
 */
function app_paths(){
    return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];
}
