<?php


namespace App\Repository;
use App\Models\Admin\AdminHasRoles;

class AdminHasRolesRepository
{
    protected static $adminHasRoles;

    public function __construct(AdminHasRoles $adminHasRoles)
    {
        self::$adminHasRoles = $adminHasRoles;
    }

    public function roles()
    {
        return self::$adminHasRoles::query()->where('status','1')->get();
    }

    /**
     * 添加用户角色
     * @param $uid
     * @param $roleIds
     * @return bool
     * @author:
     * @date: 2019/5/24 17:21
     */
    public function add($uid,$roleIds)
    {
        $data = [];
        foreach ($roleIds as $val){
            $data[] = ['uid'=>$uid,'role_id'=>$val];
        }
        return self::$adminHasRoles::query()->insert($data);
    }

    /**
     * 获取角色ids
     * @param $uid
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/30 10:27
     */
    public function getRoleIds($uid)
    {
        return self::$adminHasRoles::query()->where('uid',$uid)->get();
    }
    /**
     * 删除用户角色
     * @param $uid
     * @return mixed
     * @author:
     * @date: 2019/5/28 11:19
     */
    public function delete($uid)
    {
        if(count($this->getRoleIds($uid)) > 0 ){
            return self::$adminHasRoles::query()->where('uid',$uid)->delete();
        }
        return true;

    }
}
