<?php


namespace App\Repository;
use App\Models\Admin\Roles;

class RolesRepository
{
    protected static $roles;

    public function __construct(Roles $roles)
    {
        self::$roles = $roles;
    }

    /**
     * 角色添加
     * @param $data
     * @return mixed
     * @author:
     * @date: 2019/5/22 16:17
     */
    public function add($data)
    {
        return self::$roles::query()->add($data);
    }

    /**
     * 获取某一角色信息
     * @param $roleId
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @author:
     * @date: 2019/5/24 14:28
     */
    public function get($id)
    {
        return self::$roles::find($id);
    }

    /**
     * 修改角色
     * @param $data
     * @return bool
     * @author:
     * @date: 2019/5/24 14:46
     */
    public function save($data)
    {
        $role = self::$roles::find($data['id']);
        foreach ($data as $key=>$val){
            $role->$key = $val;
        }
        return $role->save();
    }

    /**
     * 删除角色
     * @param $roleId
     * @return bool|mixed|null
     * @throws \Exception
     * @author:
     * @date: 2019/5/24 15:06
     */
    public function delete($roleId)
    {
        return self::$roles::query()->where('id',$roleId)->delete();
    }

    /**
     * 获取所有角色
     * @return Roles[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/24 16:44
     */
    public function allRoles()
    {
        return self::$roles::get();
    }
}