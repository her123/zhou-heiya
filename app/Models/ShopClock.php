<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Shop;

class ShopClock extends Model
{
    //
    public $table="zhy_shop_clock";

    // 加上对应的字段
    protected $fillable = ['shop_id', 'shop_name','shop_code','mobile'];

    public function shop()
    {
        return $this->hasOne(Shop::class,'id','shop_id');
    }
}