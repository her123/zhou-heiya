<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * App\Models\Admin\Users
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereRememberToken($value)
 * @mixin
 *
 * Eloquent
 */
class Steps extends Model
{
    public $table = "step";

    protected $fillable = ['id','mobile','step','date','timestamp','status','created_at'];

}
