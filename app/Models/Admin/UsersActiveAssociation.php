<?php

namespace App\Models\Admin;

use App\Models\Admin\AdminHasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * App\Models\Admin\Users
 *
 * @property int $id
 * @property string $name 用户名
 * @property string $email 邮箱
 * @property string $password 密码
 * @property string $remember_token 记住token
 * @property string $login_ip 登录IP
 * @property \Illuminate\Support\Carbon $created_at 更新时间
 * @property string $login_at 最近登录时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation whereLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation whereLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActiveAssociation whereRememberToken($value)
 * @mixin
 *
 * Eloquent
 */
class UsersActiveAssociation extends Model
{
    public $table = "users_active_association";

    /**
     * 获取用户信息t_
     * @param $search
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function getUserInfo($search)
    {
        return self::where($search)->first();
    }

    /**
     * 验证用户登陆
     * @param $params
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function doLoginVerify($params)
    {
        return $this->where($params)->get();
    }

    /**
     * 获取用户列表
     * @param $where
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @author:
     * @date: 2019/5/6 20:05
     */
    public function getUsers($where = [])
    {
        return self::where($where)
            //->join('users','users.id','=','users_active_association.users_id')
            ->join('users_active','users_active.id','=','users_active_association.active_id')
            ->select(
                'users_active.active_name','users_active_association.mobile',
                'users_active_association.active_id','users_active_association.id as m_id','users_active.start_time',
                'users_active.end_time', 'users_active_association.channel', 'users_active_association.active_images'
            )
            ->orderby('users_active_association.id', 'desc')->paginate(15);
    }


    /**
     * 获取用户列表
     * @param $where
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @author:
     * @date: 2019/5/6 20:05
     */
    public function getActiveUserList($where = [])
    {
        return self::where($where)
            //->join('users','users.id','=','users_active_association.users_id')
            ->join('users_active','users_active.id','=','users_active_association.active_id')
            ->select(
                'users_active_association.avatar'
            )
            ->orderby('users_active_association.id', 'desc')->take(5)->get();
    }

    /**
     * 获取用户权限列表
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @author:
     * @date: 2019/5/22 17:43
     */
    public function hasRoles()
    {
        return $this->hasMany(AdminHasRoles::class, 'uid', 'id');
    }

    public function roles()
    {
        return $this->belongsToMany(Users::class, 'users', 'id', 'users_id');
    }

    /**
     * 获取用户所有权限
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     * @author: zhangfayuan
     * @date: 2019/7/13 13:56
     */
    public function havePermissions()
    {
        return $this->hasManyThrough(
            RoleHasPermissions::class,
            AdminHasRoles::class,
            'uid',
            'role_id',
            'id',
            'role_id'
        );
    }
}
