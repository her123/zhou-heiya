<?php

namespace App\Models\Admin;

use App\Models\Admin\AdminHasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * App\Models\Admin\Users
 *
 * @property int $id
 * @property string $name 用户名
 * @property string $email 邮箱
 * @property string $password 密码
 * @property string $remember_token 记住token
 * @property string $login_ip 登录IP
 * @property \Illuminate\Support\Carbon $created_at 更新时间
 * @property string $login_at 最近登录时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive whereLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive whereLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\UsersActive whereRememberToken($value)
 * @mixin
 *
 * Eloquent
 */
class UsersActive extends Model
{
    public $table = "users_active";

    /**
     * 获取用户信息t_
     * @param $search
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function getUserInfo($search)
    {
        return self::where($search)->first();
    }

    /**
     * 验证用户登陆
     * @param $params
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function doLoginVerify($params)
    {
        return $this->where($params)->get();
    }

    /**
     * 获取用户列表
     * @param $where
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @author:
     * @date: 2019/5/6 20:05
     */

    public function getUsers($where = [])
    {
        return self::where($where)->orderby('id', 'desc')->paginate(15);
    }

}
