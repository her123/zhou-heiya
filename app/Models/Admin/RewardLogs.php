<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * App\Models\Admin\Users
 *
 * @property int $id
 * @property string $name 用户名
 * @property string $email 邮箱
 * @property string $password 密码
 * @property string $remember_token 记住token
 * @property string $login_ip 登录IP
 * @property \Illuminate\Support\Carbon $created_at 更新时间
 * @property string $login_at 最近登录时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs whereLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs whereLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\RewardLogs whereRememberToken($value)
 * @mixin
 *
 * Eloquent
 */
class RewardLogs extends Model
{
    public $table = "reward_logs";

    /**
     * 获取用户信息t_
     * @param $search
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function getUserInfo($search)
    {
        return self::where($search)->first();
    }

    /**
     * 验证用户登陆
     * @param $params
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function doLoginVerify($params)
    {
        return $this->where($params)->get();
    }

    /**
     * 获取用户列表
     * @param $where
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @author:
     * @date: 2019/5/6 20:05
     */
    public function getUsers($where = [])
    {
        return self::where($where)
            ->join('users_active','users_active.id','=','reward_logs.active_id')
            ->select(
                'users_active.active_name','reward_logs.mobile',
                'reward_logs.active_id','reward_logs.level','users_active.start_time',
                'users_active.end_time'
            )
            ->paginate(15);
    }
}
