<?php

namespace App\Models\Admin;

use App\Models\Admin\AdminHasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * App\Models\Admin\Users
 *
 * @property int $id
 * @property string $name 用户名
 * @property string $email 邮箱
 * @property string $password 密码
 * @property string $remember_token 记住token
 * @property string $login_ip 登录IP
 * @property \Illuminate\Support\Carbon $created_at 更新时间
 * @property string $login_at 最近登录时间
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users whereLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users whereLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Users whereRememberToken($value)
 * @mixin
 *
 * Eloquent
 */
class Bank extends Model
{
    public $table = "bank";

    /**
     * 获取用户信息t_
     * @param $search
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function getBankInfo($search)
    {
        return self::where($search)->first();
    }

    /**
     * 验证用户登陆
     * @param $params
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function doLoginVerify($params)
    {
        return $this->where($params)->get();
    }

    /**
     * 获取用户列表
     * @param $where
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @author:
     * @date: 2019/5/6 20:05
     */

    public function getBanks($where = [])
    {
        return self::where($where)->orderby('id', 'desc')->paginate(15);
    }

    /**
     * 获取用户权限列表
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @author:
     * @date: 2019/5/22 17:43
     */
    public function hasUser()
    {
        return $this->hasMany(Users::class, 'uid', 'id');
    }

    public function banks()
    {
        return $this->belongsToMany(Users::class, 'bank', 'uid', 'user_id');
    }

    /**
     * 获取用户所有权限
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     * @author: zhangfayuan
     * @date: 2019/7/13 13:56
     */
    public function havePermissions()
    {
        return $this->hasManyThrough(
            RoleHasPermissions::class,
            AdminHasRoles::class,
            'uid',
            'role_id',
            'id',
            'role_id'
        );
    }
}
