<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * App\Models\Admin\Users
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Step whereRememberToken($value)
 * @mixin
 *
 * Eloquent
 */
class Step extends Model
{
    public $table = "step";

    /**
     * 获取用户信息t_
     * @param $search
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function getStepInfo($search)
    {
        return self::where($search)->first();
    }


    /**
     * 获取用户列表
     * @param $where
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @author:
     * @date: 2019/5/6 20:05
     */

    public function getStep($where = [])
    {
        return self::where($where)->orderby('id', 'desc')->paginate(15);
    }

    /**
     * 获取积分商城的总商品数
     * @param array $where
     * @param array $time
     * @return
     */
    public function getStepCount($where=[],$time=[])
    {
        return self::where($where)->whereBetween('date', $time)->count();
    }

    /**
     * 获取积分商品列表
     * @param $skip
     * @param $perPage
     * @param array $where
     * @param array $time
     * @return
     */
    public function getAllStep($skip,$perPage,$where=[],$time=[])
    {
        return self::where($where)
            ->whereBetween('date', $time)->orderby('id', 'asc')->skip($skip)->take($perPage)->get();
    }
}
