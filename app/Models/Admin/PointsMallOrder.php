<?php

namespace App\Models\Admin;

use App\Models\Shop;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * App\Models\Admin\Users
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder whereLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder whereLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\PointsMallOrder whereRememberToken($value)
 * @mixin
 *
 * Eloquent
 */
class PointsMallOrder extends Model
{
    public $table = "points_mall_order";

    /**
     * 获取用户信息t_
     * @param $search
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function getMallInfo($search)
    {
        return self::where($search)->first();
    }

    /**
     * 验证用户登陆
     * @param $params
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function doLoginVerify($params)
    {
        return $this->where($params)->get();
    }

    /**
     * 获取用户列表
     * @param $where
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @author:
     * @date: 2019/5/6 20:05
     */

    public function getMall($where = [])
    {
        return self::where($where)->orderby('id', 'desc')->paginate(15);
    }

    /**
     * 获取用户权限列表
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @author:
     * @date: 2019/5/22 17:43
     */
    public function hasUser()
    {
        return $this->hasMany(Users::class, 'uid', 'id');
    }

    public function banks()
    {
        return $this->belongsToMany(Users::class, 'bank', 'uid', 'user_id');
    }

    /**
     * 获取积分商城的总商品数
     * @param array $where
     * @return
     */
    public function getMallOrderCount($where=[])
    {
        return self::where($where)->count();
    }


    public function getMallOrderSum($where=[])
    {
        return self::where($where)->sum('mobile_commission');
    }

    /**
     * 获取积分商品列表
     * @param $skip
     * @param $perPage
     * @param array $where
     * @return
     */
    public function getAllMalls($skip,$perPage,$where=[])
    {
        return self::where($where)
            ->select('id','title','images','point','money',)
            ->orderby('points_mall.id', 'desc')->skip($skip)->take($perPage)->get(15);
    }


    public function mall()
    {
        return $this->hasOne(PointsMall::class,'id','mall_id');
    }

}
