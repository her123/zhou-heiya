<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    //
    protected $table = 'permissions';

    /**
     * 查询可访问的路由
     * @param $ids
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/30 17:12
     */
    public function getPermissions($ids)
    {
        return $this::query()->select(['module','controller','action'])->whereIn('id',$ids)->get();
    }
}
