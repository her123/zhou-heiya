<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * App\Models\Admin\Users
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent whereLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent whereLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Agent whereRememberToken($value)
 * @mixin
 *
 * Eloquent
 */
class Agent extends Model
{
    public $table = "agent";

    /**
     * 获取用户信息t_
     * @param $search
     * @return mixed
     * @author:
     * @date: 2019/4/29 14:42
     */
    public function getAgentInfo($search)
    {
        return self::where($search)->first();
    }


    /**
     * 获取用户列表
     * @param $where
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @author:
     * @date: 2019/5/6 20:05
     */

    public function getAgent($where = [])
    {
        return self::where($where)->orderby('id', 'desc')->paginate(15);
    }

    /**
     * 获取积分商城的总商品数
     * @param array $where
     * @return
     */
    public function getAgentTotalCount($where=[])
    {
        return self::where($where)->count();
    }

    public function getAgentTodayCount($where=[]){
        return self::where($where)->whereBetween('created_at',[date('Y-m-d 00:00:00', time()),date('Y-m-d 23:59:59', time())])->count();
    }

    /**
     * 获取积分商品列表
     * @param $skip
     * @param $perPage
     * @param array $where
     * @return
     */
    public function getAllAgent($skip,$perPage,$where=[])
    {
        return self::where($where)
            ->orderby('id', 'desc')->skip($skip)->take($perPage)->get();
    }
}
