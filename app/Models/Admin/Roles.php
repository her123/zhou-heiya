<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Admin\Roles
 *
 * @property int $id
 * @property string $name 角色名称
 * @property \Illuminate\Support\Carbon $created_at 创建时间
 * @property \Illuminate\Support\Carbon $updated_at 更新时间
 * @property int $status 状态 1 开启 0 关闭
 * @property string $descriptions 角色简介
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Roles newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Roles newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Roles query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Roles whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Roles whereDescriptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Roles whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Roles whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Roles whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Roles whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Roles extends Model
{
    //
    public $table = "roles";

    /**
     * 角色列表
     * @param array $where
     * @return mixed
     * @author:
     * @date: 2019/5/6 15:06
     */
    public function getAllRoles($where = [])
    {
        return self::where($where)->orderBy('id','ASC')->paginate(5);
    }

    /**
     * 角色添加
     * @param $params
     * @return mixed
     * @author:
     * @date: 2019/5/6 15:06
     */
    public function add($params)
    {
        return self::insert($params);
    }
}
