<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderSkApp
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSkApp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSkApp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSkApp query()
 * @mixin \Eloquent
 */
class OrderSkApp extends Model
{
    //tabel
    public $table = "order_sk_app";

}
