<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ShopCoupon extends Model
{
    //
    public $table="zhy_shop_coupon";

    // 加上对应的字段
    protected $fillable = ['coupon_code', 'coupon_num','mobile'];


}