<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    //

    public $table="shop";

    // 加上对应的字段
    protected $fillable = ['shop_code','shop_name','shop_area','shop_lng','shop_lat'];
}
