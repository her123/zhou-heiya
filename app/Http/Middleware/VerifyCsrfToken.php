<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'http://localhost:8088/*',
        'http://livetake.com:8088/*',
        'http://zhouheiya.gdweihu.com/*',
        'https://zhouheiya.gdweihu.com/*',
        'http://www.zhouheiya.cn/*',
        'http://www.zhouheiya.com.cn/*',
    ];
}
