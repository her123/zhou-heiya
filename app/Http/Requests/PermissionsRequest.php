<?php


namespace App\Http\Requests;


class PermissionsRequest extends Request
{
    public function rules()
    {
        return [
            'name'      => 'required|unique:permissions,name,' . $this->request->get('id'),
            'pid'       => 'required|integer',
            'is_show'   => 'required|between:1,2'
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => '菜单名称必填',
            'name.unique'       => '菜单名称已存在',
            'pid.required'      => '父级菜单必选',
            'pid.integer'       => '父级菜单格式不正确',
            'is_show.required'  => '是否显示必选',
            'is_show.between'   => '是否显示格式不正确',
        ];
    }
}