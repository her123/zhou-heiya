<?php


namespace App\Http\Requests;


use App\Models\Admin\Users;
use App\Service\UsersService;

class UsersRequest extends Request
{
    public function rules()
    {
        return  [
            'name'          => 'sometimes|required|min:4|max:16|regex:/^[a-zA-Z0-9_-]{4,16}$/|unique:users,name,'.$this->request->get('id'),
            'email'         => 'sometimes|required|regex:/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/|unique:users,email,'.$this->request->get('id'),
            'mobile'        => 'sometimes|required|regex:/^1[34578][0-9]{9}$/|unique:users,mobile,'.$this->request->get('id'),
            'sex'           => 'sometimes|required|integer|between:1,2',
            'password'      => 'sometimes|confirmed',
            'password_confirmation' => 'sometimes|same:password',
            'withdrawal_password'      => 'sometimes|confirmed',
            'withdrawal_password_confirmation' => 'sometimes|same:withdrawal_password',
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => '用户名必填',
            'name.min'          => '用户名最少要四个字符长度',
            'name.max'          => '用户名最大不得超过十六个字符',
            'name.unique'       => '用户名已存在',
            'name.regex'        => '用户名格式不正确',
            'email.required'    => '邮箱地址必填',
            'email.regex'       => '邮箱格式不正确',
            'email.unique'      => '邮箱已存在',
            'mobile.required'   => '手机号码必填',
            'mobile.regex'      => '手机号码格式不正确',
            'mobile.unique'     => '手机号已存在',
            'sex.required'      => '性别必选',
            'sex.integer'       => '性别必须是整型',
            'sex.between'       => '性别必须在1和2之间',
            'password.required' => '账户密码必填',
            'password.confirmed'=> '确认密码与账户密码不一致',
            'withdrawal_password.required' => '提现密码必填',
            'withdrawal_password.confirmed'=> '确认提现密码与提现密码不一致',
        ];
    }
}
