<?php


namespace App\Http\Requests;

use App\Models\Admin\PointsMall;
use App\Service\PointsMallService;

class PointsMallRequest extends Request
{
    public function rules()
    {
        return  [
            'title'          => 'sometimes|required|max:100|unique:points_mall,title,'.$this->request->get('id'),
            //'point'        => 'sometimes|required'.$this->request->get('id'),
//            'point'        => 'sometimes|required|regex:/[0-9]/'.$this->request->get('id'),
            //'money'           => 'sometimes|required|decimal',
        ];
    }

    public function messages()
    {
        return [
            'title.required'     => '积分商品标题必填',
            'title.max'          => '积分商品标题太长',
            'title.unique'       => '积分商品标题已存在',
            //'point.required'   => '积分必填',
            //'point.regex'      => '积分格式不正确',
            //'money.required'      => '性别必选',
            //'money.decimal'       => '金额格式不正确',
//            'password.required' => '账户密码必填',
//            'password.confirmed'=> '确认密码与账户密码不一致',
//            'withdrawal_password.required' => '提现密码必填',
//            'withdrawal_password.confirmed'=> '确认提现密码与提现密码不一致',
        ];
    }
}
