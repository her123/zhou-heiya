<?php


namespace App\Http\Requests;


use App\Models\Admin\UsersActiveAssociation;
use App\Service\UsersActiveAssociationService;

class UsersActiveAssociationRequest extends Request
{
    public function rules()
    {
        return  [
            'user_id'           => 'sometimes|required',
            'active_id'         => 'sometimes|required',

        ];
    }

    public function messages()
    {
        return [
            'user_id.required'     => '用户必须填写',
            'active_id'        => '活动开始时间必填',
        ];
    }
}
