<?php


namespace App\Http\Requests;


use App\Models\Admin\StepReward;
use App\Service\StepRewardService;

class StepRewardRequest extends Request
{
    public function rules()
    {
        return  [
            'month'         => 'sometimes|required|integer|between:1,12|unique:step_reward,month,'.$this->request->get('id'),
            'content'       => 'sometimes|required',
        ];
    }

    public function messages()
    {
        return [
            'month.required'    => '月份必选',
            'month.integer'     => '月份必须是整型',
            'month.between'     => '月份必须在一月和十二月之间',
            'content.required'  => '奖励说明必填',
        ];
    }
}
