<?php


namespace App\Http\Requests;


use App\Models\Admin\UsersActive;
use App\Service\UsersActiveService;

class UsersActiveRequest extends Request
{
    public function rules()
    {
        return  [
            'name'          => 'sometimes|required|unique:users_active,name,'.$this->request->get('id'),
            'start_time'    => 'sometimes|required',
            'end_time'      => 'sometimes|required',
            'platform'      => 'sometimes|required',
            'mechanism'     => 'sometimes|required',

        ];
    }

    public function messages()
    {
        return [
            'name.required'     => '用户名必填',
            'name.unique'       => '用户名已存在',
            'start_time'    => '活动开始时间必填',
            'end_time'      => '活动结束时间必填',
            'platform'      => '活动平台必填',
            'mechanism'     => '活动介绍机制必填',
        ];
    }
}
