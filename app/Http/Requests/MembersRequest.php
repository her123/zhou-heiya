<?php


namespace App\Http\Requests;


use App\Models\Admin\Users;
use App\Service\UsersService;

class MembersRequest extends Request
{
    public function rules()
    {
        return  [
            'name'          => 'sometimes|required,'.$this->request->get('id'),
            'birthday'      => 'sometimes|required,'.$this->request->get('id'),
            'mobile'         => 'sometimes|required|regex:/^1[34578][0-9]{9}$/|unique:users,mobile,'.$this->request->get('id'),
            'sex'           => 'sometimes|required|integer|between:1,2',
            'channel'       => 'sometimes|required',
            'shopId'        => 'sometimes|required',
            'userUniqueCode'=> 'sometimes|required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => '用户名必填',
            'mobile.required'   => '手机号码必填',
            'mobile.regex'      => '手机号码格式不正确',
            'mobile.unique'     => '手机号已存在',
            'sex.required'      => '性别必选',
            'sex.integer'       => '性别必须是整型',
            'sex.between'       => '性别必须在1和2之间',
            'channel.required' => '注册渠道必填',
            'shopId.required' => 'shopId必填',
            'userUniqueCode.required' => '用户唯一标识码必填',
        ];
    }
}
