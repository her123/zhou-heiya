<?php

namespace App\Http\Controllers;

use App\Service\HttpService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Log;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     *
     * @param HttpService $httpService
     * @param $mobile
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2020-09-02 16:40
     *
     * 判断用户是否注册
     */
    public function isUser($httpService,$mobile)
    {
        $url = config('app.url').'member_check?mobile='.$mobile;

        try {
            $res = $httpService->HttpRequest($url, 'GET');
        }catch (\Exception $e){
            return tips(-1,'error',$e->getMessage());
        } catch (GuzzleException $e) {
            return tips(-1,'error',$e->getMessage());
        }

        return $res;

    }

    /**
     *
     * @param $httpService
     * @param $str 手机号、openId、虚拟卡号（其中之一，对应下面的querryType）
     * @param $type  1-根据openId查询，2-手机号码查询，3-虚拟卡号查询
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2020-09-03 13:46
     *
     *
     */
    public function getUser($httpService,$str,$type)
    {
        $queryStr = $str;
        $queryType = $type;
        $params['querryStr'] = $queryStr['queryStr'];
        $params['querryType'] = $queryType['queryType'];
        $params['sign'] = strtoupper(MD5("&querryStr=" . $queryStr['queryStr'] . "&querryType=" . $queryType['queryType']
            . "&key=" . config('app.zhy_config.key')));

        $params = http_build_query(($params));
        $url = config('app.zhy_config.crm_member_query') . '?' . $params;
        try {
            $data = $httpService->HttpRequest($url, 'GET');

        } catch (\Exception $e) {
            return $this->errorReturn(10001, $e->getMessage());
        } catch (GuzzleException $e) {

            return $this->errorReturn(10001, $e->getMessage());
        }

        //“-1”-失败;0-该手机号码未注册会员；1-该手机号码注册了微信会员;2-该手机号码注册了支付宝会员;3-该手机号码同时注册了支付宝会员和微信会员
        if (isset($data['statu'])) {
            if ($data['statu'] == -1) {
                return tips($data['statu'], 'error', $data['Msg']);
            }
            return $data;
        }else{
            return tips(-1, 'error','请求失败');
        }


    }


    function curl_open($url, $json_data){
        $ch = curl_init();
        $timeout  = 1;
        curl_setopt( $ch , CURLOPT_TIMEOUT, $timeout );
        curl_setopt( $ch , CURLOPT_CONNECTTIMEOUT_MS, 200);
        curl_setopt( $ch , CURLOPT_HEADER, false);
        curl_setopt( $ch , CURLOPT_URL, $url);
        curl_setopt( $ch , CURLOPT_RETURNTRANSFER, 1);
        curl_setopt( $ch , CURLOPT_POST, 1);
        curl_setopt( $ch , CURLOPT_POSTFIELDS, $json_data);
        $output = curl_exec($ch);
        curl_close($ch);
        if($output!=""){
            $val = json_decode($output,true);
            Log::info('周黑鸭的测试数据',$val); //日志监控
            if($val['errcode'] == 0){
                return array("errcode"=>0, "errmsg"=>"", "rtval"=>$val);
            }else{
                return array("errcode"=>$val['errcode'], "errmsg"=>$val['errstr'], "rtval"=>null);
            }
        }else{
            return array("errcode"=>4002, "errmsg"=>"WCS Timeout", "rtval"=>null);
        }


    }

    /**
     *
     * @param HttpService $httpService
     * @return array|bool|false|float|int|object|string|null
     * @throws GuzzleException
     * @author Bryant
     * @date 2020-09-07 20:20
     *
     * 获取 access_token
     */
    public function getAccent(HttpService $httpService)
    {
        $appid = config('app.wechat_miniprogram_config.appid');//配置appid
        $secret = config('app.wechat_miniprogram_config.secret');//配置secret
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$appid}&secret={$secret}";

        $res = $httpService->HttpRequest($url, 'GET');

        return $res;
    }


    /**
     * 获取用户信息 使用该方法时必须验证token
     */
    /*protected function userInfo(){

        return auth()->user('api')->id;
    }*/

    /* header('Content-Type:text/html;charset=utf-8');

       $url = "http://wdqas01.zhouheiya.cn:50000/dir/wsdl?p=ic/27fa8ddb5d5230ae920d01a4d7ea7ca8";
       $client = new \SoapClient($url,array('encoding'=>"gb2312",'location'=>'http://WDQAS01.zhouheiya.cn'));
       dd($client->__getFunctions());
       $parms = array(
           'PARTNER' => "hujun",
       );*/


    /*  //header('Content-Type:text/html;charset=utf-8');f

      $ws = "http://wdqas01.zhouheiya.cn:50000/dir/wsdl?oid=080ec8e04acb320f810685567fc9d835";
      //$ws = "http://wdqas01.zhouheiya.cn:50000/dir/wsdl?p=ic/27fa8ddb5d5230ae920d01a4d7ea7ca8";

      //$ws = "http://ws.webxml.com.cn/WebServices/WeatherWS.asmx?wsdl";//webservice服务的地址
      $client = new SoapClient($ws);
      $client ->debugLastSoapRequest();
      dd($client);
      $result=$client->getWeather(array('theCityCode'=>'广州','theUserID'=>''));//查询中国深圳的天气，返回的是一个结构体print_r($result);
      dd($result);
      phpinfo();*/

    // 用户id
    //$memberId = $this->userInfo();




    /**
     *
     * @author Bryant
     * @date 2020-10-21 14:37
     *
     * 获取会员卡号
     */
    public function getMemberCard(HttpService $httpService,$str='')
    {
        $queryStr = $str;
        if (!$queryStr) {
            return tips('error', '参数错误');
        }

        $params['querryStr'] = $queryStr['queryStr'];
        $params['sign'] = strtoupper(MD5("&querryStr=" . $queryStr['queryStr'] . "&key=" . config('app.zhy_config.key')));

        $params = http_build_query(($params));
        $url = config('app.zhy_config.member_query') . '?' . $params;
        Log::debug('memberQuery：queryStr---' . json_encode($queryStr['queryStr']));

        try {
            $data = $httpService->HttpRequest($url, 'GET');
        } catch (\Exception $e) {
            return $this->errorReturn(10001, $e->getMessage());
        } catch (GuzzleException $e) {
            return $this->errorReturn(10001, $e->getMessage());
        }

        //“-1”-失败;0-该手机号码未注册会员；1-该手机号码注册了微信会员;2-该手机号码注册了支付宝会员;3-该手机号码同时注册了支付宝会员和微信会员
        if (isset($data['Statu'])) {
            if ($data['Statu'] == -1) {
                return tips($data['Statu'], 'error', $data['Msg']);
            } else {
                return $data;
            }
        }
        return $data;
    }

    /**
     * 5.周黑鸭查询会员（返回余额积分）
     * @param HttpService $httpService
     * @return false|\Illuminate\Http\JsonResponse|string
     */
    public function memberQueryWithBalance(HttpService $httpService,$str = '')
    {
        $queryStr = $str;
        if (!$queryStr) {
            return tips(-1, 'error', '参数错误');
        }

        $params['querryStr'] = $queryStr['queryStr'];
        $params['sign'] = strtoupper(MD5("&querryStr=" . $queryStr['queryStr'] . "&key=" . config('app.zhy_config.key')));

        $params = http_build_query(($params));
        $url = config('app.zhy_config.member_query_with_balance') . '?' . $params;

        try {
            $data = $httpService->HttpRequest($url, 'GET');
        } catch (\Exception $e) {
            return $this->errorReturn(10001, $e->getMessage());
        } catch (GuzzleException $e) {
            return $this->errorReturn(10001, $e->getMessage());
        }

        return $data;
    }


}

