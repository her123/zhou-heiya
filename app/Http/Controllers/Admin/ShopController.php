<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BankRequest;
use App\Models\Admin\Users;
use App\Models\ShopAccount;
use App\Models\ShopClock;
use App\Repository\RolesRepository;
use App\Repository\AdminRepository;
use App\Http\Requests\UsersRequest;
use App\Models\Shop;
use App\Service\UsersService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use mysql_xdevapi\Exception;
use App\Service\BankService;

use App\Imports\AdminsImport;

use Maatwebsite\Excel\Facades\Excel;

class ShopController extends BaseController
{
    /**
     * 管理员列表
     * @param Bank $users
     * @return Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/17 15:22
     */
    public function index(Request $request, Shop $shop)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['shop_name', 'like', '%' . $params['keywords'] . '%'],
            ];
        }
        $shopList = $shop->where($where)->orderby('id', 'desc')->paginate(15);
        return view('admin.shop.index', [
            'request' => $request,
            'search' => $params,
            'shopList' => $shopList
        ]);
    }

    /**
     *  添加用户模板
     * @param UsersService $usersService
     * @return Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/20 19:14
     */
    public function add(UsersService $usersService)
    {
        return view('admin.shop.add',[
            'users' => $usersService->allUsers(),
        ]);
    }

    /**
     * 添加用户操作
     * @param BankRequest $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/24 18:00
     */
    public function store(Request $request,Shop $shop)
    {
        $params = $request->except(['_token']);
        DB::beginTransaction();
        try {
            $addShop = $shop->insert($params);
            if (!$addShop) {
                throw new \Exception('数据库操作异常');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorReturn(10001, $e->getMessage());
        }
        DB::commit();
        return $this->successReturn();
    }

    /**
     * 用户详情
     * @param $id
     * @param BankService $bankService
     * @return Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/20 19:15
     */
    public function edit($id,Shop $shop)
    {
        $shopInfo = $shop->find($id);
        return view('admin.shop.edit', [
            'shopInfo' => $shopInfo,
        ]);
    }

    /**
     * 更新用户信息
     * @param BankRequest $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/20 19:50
     */
    public function update(Request $request,Shop $shop)
    {
        $params = $request->except(['_token']);
        if ($params['id']) {
            DB::beginTransaction();
            try {
                $updateUsers = $shop->where('id',$params['id'])->update($params);
                if (!$updateUsers) {
                    throw new \Exception('门店信息编辑出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();
        }
        return $this->errorReturn(10001, '编辑出错');

    }

    /**
     * 删除用户信息
     * @param Request $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/20 20:27
     */
    public function delete(Request $request,Shop $shop)
    {
        return $shop->where('id',$request->id)->delete() ? $this->successReturn() : $this->errorReturn();
    }


    /**
     *
     * @author Bryant
     * @date 2020-10-10 18:09
     *
     * 导入页面
     */
    public function addImport()
    {
        return view('admin.shop.import');
    }


    /**
     *
     * @author Bryant
     * @date 2020-10-10 18:14
     *
     *
     */
    public function storeImport(Request $request)
    {
        $file = $request->file('shop_file');

        DB::beginTransaction();
        try {
            $data = Excel::import(new AdminsImport(),$file);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorReturn(10001, $e->getMessage());
        }
        DB::commit();
        return $this->successReturn();

    }

    /**
     *
     * @author Bryant
     * @date 2020-10-16 14:42
     * 用户打卡列表
     */
    public function getUserShop(Request $request,ShopClock $shopClock)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['mobile', 'like', '%' . $params['keywords'] . '%'],
            ];
        }
        $shopList = $shopClock->where($where)->with('shop')->orderby('id', 'desc')->paginate(15);

        return view('admin.shop.indexs', [
            'request' => $request,
            'search' => $params,
            'shopList' => $shopList
        ]);
    }

    /**
     *
     * @return Factory|\Illuminate\View\View
     * @author Bryant
     * @date 2020-10-23 14:58
     * 门店账号的编辑和更改
     */
    public function shopAccount(Request $request)
    {
        $id = $request->id;
        $info = ShopAccount::where('shop_id',$id)->first();

        if($info){
            return view('admin.shop.accounts',[
                'shop_id' => $id,
                'info' => $info ?? []
            ]);
        }else{
            return view('admin.shop.account',[
                'shop_id' => $id,
            ]);
        }

    }

    /**
     *
     * @author Bryant
     * @date 2020-10-23 16:18
     *
     * 操作编辑和修改
     */
    public function shopSave(Request $request,ShopAccount $shopAccount)
    {


        $params = $request->except(['_token']);

        if ($params['password'] != $params['new_password']) {

            return $this->errorReturn(10001, '两次密码不一致');
        }
        $params = $request->except(['_token','new_password']);

        $params['password'] = Hash::make( $params['password']);
/*
        $shopAccount -> shop_id = $params['shop_id'];

        $shopAccount -> account = $params['account'];

        $shopAccount -> password = $params['password'];

        $shopAccount -> created_at = date('Y-m-d H:i:s',time());*/

        $info = $shopAccount->where('shop_id',$params['shop_id'])->first();

        if ($info) {

            DB::beginTransaction();
            try {
                $updateUsers = $shopAccount->where('shop_id',$params['shop_id'])->update($params);
                if (!$updateUsers) {
                    throw new \Exception('门店信息编辑出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();
        } else {
            DB::beginTransaction();
            try {
                $updateUsers = $shopAccount->where('shop_id',$params['shop_id'])->insert($params);
                if (!$updateUsers) {
                    throw new \Exception('门店信息新增出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();

        }
        return $this->errorReturn(10001, '新增出错');
    }


}
