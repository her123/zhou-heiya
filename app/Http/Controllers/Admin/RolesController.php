<?php


namespace App\Http\Controllers\Admin;

use App\Http\Requests\RolesRequest;
use App\Models\Admin\Roles;
use App\Repository\PermissionsRepository;
use App\Repository\RoleHasPermissionsRepository;
use App\Repository\RolesRepository;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

class RolesController extends BaseController
{
    /**
     * 角色管理主页
     * @param Request $request
     * @param Roles $roles
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/4/30 13:25
     */
    public function index(Request $request,Roles $roles)
    {
        $where = [];
        $params = $request->except('_token');
        if(isset($params['name'])){
            $where = [
                ['name','like','%'.$params['name'].'%']
            ];
        }
        $roles = $roles->getAllRoles($where);
        return view('admin.roles.index',[
            'roles'=>$roles,
            'search'=>$params,
            'request'=>$request
        ]);
    }

    /**
     * 角色添加
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/4/30 15:33
     */
    public function add()
    {
        return view('admin.roles.add');
    }

    /**
     * 添加角色操作
     * @param RolesRequest $request
     * @param Roles $roles
     * @return false|string
     * @author:
     * @date: 2019/5/24 14:18
     */
    public function addRoles(RolesRequest $request,Roles $roles)
    {
        $params = $request->except('_token');
        return $roles->add($params) ? $this->successReturn() : $this->errorReturn(10001,'角色添加失败');
    }

    /**
     * 角色信息编辑
     * @param $roleId
     * @param RolesRepository $rolesRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/24 14:29
     */
    public function edit($id,RolesRepository $rolesRepository)
    {
        return view('admin.roles.edit',[
            'roleInfo' => $rolesRepository->get($id),
        ]);
    }

    public function save(RolesRequest $request,RolesRepository $rolesRepository)
    {
        $params = $request->except('_token');
        if($rolesRepository->save($params) !== false){
            return $this->successReturn();
        }
        return $this->errorReturn();
    }

    /**
     * 删除角色
     * @param Request $request
     * @param RolesRepository $rolesRepository
     * @param RoleHasPermissionsRepository $roleHasPermissionsRepository
     * @return false|string
     * @throws Exception
     * @author:
     * @date: 2019/5/24 15:42
     */
    public function delete(Request $request,RolesRepository $rolesRepository,RoleHasPermissionsRepository $roleHasPermissionsRepository)
    {
        DB::beginTransaction();
        try{
            $deleteRole         = $rolesRepository->delete($request->id);
            $deletePermissions  = $roleHasPermissionsRepository->delete($request->id);
            if($deletePermissions == false || !$deleteRole){
                throw new Exception('数据库操作异常');
            }
        }catch (Exception $e){
            DB::rollBack();
            return $this->errorReturn(10001,$e->getMessage());
        }
        DB::commit();
        return $this->successReturn();

    }

    /**
     * 权限设置
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/23 18:50
     */
    public function permissions(Request $request)
    {
        return view('admin.roles.permissions',[
            'role_id'=>$request->role_id
        ]);
    }

    /**
     * 角色添加权限
     * @param Request $request
     * @param RoleHasPermissionsRepository $roleHasPermissionsRepository
     * @return false|string
     * @author:
     * @date: 2019/5/24 10:28
     */
    public function permissionsAdd(Request $request,RoleHasPermissionsRepository $roleHasPermissionsRepository)
    {
        $params = $request->except('_token');
        if($roleHasPermissionsRepository->delete($params['role_id']) !== false){
            $data = [];
            foreach ($params['permissions'] as $val){
                $data[] = ['role_id'=>$params['role_id'],'permission_id'=>$val];
            }
            return $roleHasPermissionsRepository->add($data) ? $this->successReturn() : $this->errorReturn();
        }else{
            return $this->errorReturn(10002,'数据库操作异常');
        }
    }

    /**
     * 获取权限
     * @param PermissionsRepository $permissionsRepository
     * @return array
     * @author:
     * @date: 2019/5/23 18:54
     */
    public function getPermissions(Request $request,PermissionsRepository $permissionsRepository,RoleHasPermissionsRepository $roleHasPermissionsRepository){
        $roleHasPermissions = $roleHasPermissionsRepository->get('permission_id',['role_id'=>$request->role_id])->toArray();
        $field = ['id','pid','name'];
        $permissions = $permissionsRepository->getPermissions($field)->toArray();
        foreach ($permissions as $key=>$val){
            if($val['pid'] == 0){
                $permissions[$key]['open'] = true;
            }
            if(in_array($val['id'],$roleHasPermissions)){
                $permissions[$key]['checked'] = true;
                $permissions[$key]['open'] = true;

            }
        }
        return $permissions;
    }
}