<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BankRequest;
use App\Models\Admin\Users;
use App\Repository\RolesRepository;
use App\Repository\AdminRepository;
use App\Http\Requests\UsersRequest;
use App\Models\Admin\Bank;
use App\Service\UsersService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use App\Service\BankService;

class BankController extends BaseController
{
    /**
     * 管理员列表
     * @param Bank $users
     * @return Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/17 15:22
     */
    public function index(Request $request, Bank $users)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['name', 'like', '%' . $params['keywords'] . '%', 'or'],
                ['mobile', 'like', '%' . $params['keywords'] . '%', 'or'],
                ['email', 'like', '%' . $params['keywords'] . '%', 'or']
            ];
        }
        $usersList = $users->getBanks($where);
        return view('admin.bank.index', [
            'request' => $request,
            'search' => $params,
            'usersList' => $usersList
        ]);
    }

    /**
     *  添加用户模板
     * @param UsersService $usersService
     * @return Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/20 19:14
     */
    public function add(UsersService $usersService)
    {
        return view('admin.bank.add',[
            'users' => $usersService->allUsers(),
        ]);
    }

    /**
     * 添加用户操作
     * @param BankRequest $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/24 18:00
     */
    public function add_users(BankRequest $request, BankService $bankService)
    {
        $params = $request->except(['_token']);
        DB::beginTransaction();
        try {
            $addUsers = $bankService->store($params);
            if (!$addUsers) {
                throw new \Exception('数据库操作异常');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorReturn(10001, $e->getMessage());
        }
        DB::commit();
        return $this->successReturn();
    }

    /**
     * 用户详情
     * @param $id
     * @param BankService $bankService
     * @return Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/20 19:15
     */
    public function edit($id, BankService $bankService,UsersService $usersService)
    {
        $userInfo = $bankService->get($id);
        return view('admin.bank.edit', [
            'bankInfo' => $userInfo,
            'users' => $usersService->allUsers(),
        ]);
    }

    /**
     * 更新用户信息
     * @param BankRequest $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/20 19:50
     */
    public function update(BankRequest $request, BankService $bankService)
    {
        $params = $request->except(['_token']);
        if ($params['id']) {
            DB::beginTransaction();
            try {
                $updateUsers = $bankService->update($params);
                if (!$updateUsers) {
                    throw new \Exception('用户信息编辑出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();
        }
        return $this->errorReturn(10001, '编辑出错');

    }

    /**
     * 删除用户信息
     * @param Request $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/20 20:27
     */
    public function delete(Request $request, BankService $bankService)
    {
        return $bankService->delete($request->id) ? $this->successReturn() : $this->errorReturn();
    }

    /**
     * 用户拉黑
     * @param Request $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/22 15:42
     */
    public function pullBack(Request $request, BankService $bankService)
    {
        $userStatus = 1;
        if ($request->post('type') == 1) {
            $userStatus = 0;
        }

        return $bankService->pullBlack($request->id, $userStatus) ? $this->successReturn() : $this->errorReturn(10001, '操作异常');
    }

}
