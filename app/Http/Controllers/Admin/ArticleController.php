<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StepRewardRequest;
use App\Models\Article;
use App\Service\StepRewardService;
use App\Service\UsersService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends BaseController
{

    public function index(Request $request, Article $article)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['title', 'like', '%' . $params['keywords'] . '%', 'or'],
            ];
        }
        $articleList = $article->where($where)->orderby('id', 'desc')->paginate(15);
        return view('admin.article.index', [
            'request' => $request,
            'search' => $params,
            'articleList' => $articleList
        ]);
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Bryant
     * @date 2020-09-09 16:12
     *
     * 添加优惠券
     */
    public function add()
    {
        return view('admin.article.add');
    }


    /**
     *
     * @author Bryant
     * @date 2020-09-09 16:13
     *
     * 添加
     */
    public function store(Request $request,Article $article)
    {
        $params = $request->except(['_token']);

        DB::beginTransaction();
        try {
            $addUsers = $article->insert($params);
            if (!$addUsers) {
                throw new \Exception('数据库操作异常');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorReturn(10001, $e->getMessage());
        }
        DB::commit();
        return $this->successReturn();

    }




    /**
     * 编辑页
     * @param $id
     * @param StepRewardService $stepRewardService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Article $article)
    {
        $articleInfo = $article->find($id);
        return view('admin.article.edit', [
            'articleInfo' => $articleInfo,
        ]);
    }

    /**
     * 更新用户信息
     * @param StepRewardRequest $request
     * @param StepRewardService $stepRewardService
     * @return false|string
     * @author:
     * @date: 2019/5/20 19:50
     */
    public function update(Request $request,Article $article)
    {
        $params = $request->except(['_token']);

        if ($params['id']) {
            DB::beginTransaction();
            try {
                $updateUsers = $article->where('id',$params['id'])->update($params);
                if (!$updateUsers) {
                    throw new \Exception('编辑出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();
        }
        return $article->where('id',$params['id'])->update($params) ? $this->successReturn() : $this->errorReturn(10001, '编辑出错');

    }

    /**
     *
     * @param Request $request
     * @param UsersService $usersService
     * @return false|string
     * @author Bryant
     * @date 2020-09-09 19:20
     *
     * s
     */
    public function delete(Request $request,Article $article)
    {
        return $article->where('id',$request->id)->delete() ? $this->successReturn() : $this->errorReturn();
    }



}
