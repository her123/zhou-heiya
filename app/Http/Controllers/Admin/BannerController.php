<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StepRewardRequest;
use App\Models\Banner;
use App\Service\StepRewardService;
use App\Service\UploadsService;
use App\Service\UsersService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BannerController extends BaseController
{

    public function index(Request $request, Banner $banner)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['title', 'like', '%' . $params['keywords'] . '%', 'or'],
            ];
        }
        $bannerList = $banner->where($where)->orderby('id', 'desc')->paginate(15);
        return view('admin.banner.index', [
            'request' => $request,
            'search' => $params,
            'bannerList' => $bannerList
        ]);
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Bryant
     * @date 2020-09-09 16:12
     *
     * 添加优惠券
     */
    public function add()
    {
        return view('admin.banner.add');
    }


    /**
     *
     * @author Bryant
     * @date 2020-09-09 16:13
     *
     * 添加
     */
    public function store(Request $request,Banner $banner,UploadsService $uploadsService)
    {
        $params = $request->except(['_token']);
        // 必须上传积分商品图片
        if(!$request->img_url){
            return $this->errorReturn(10001, '请选择图片上传');
        }

        $path = $uploadsService->uploads_img($request->img_url);
        if(isset($path->original['status_code']) == 'error'){
            return $this->errorReturn(10001,$path->original['data']);
        }

        $params['img_url'] = $path;
        //图片文件字段要unset掉
        unset($params['file']);
        DB::beginTransaction();
        try {
            $addUsers = $banner->insert($params);
            if (!$addUsers) {
                throw new \Exception('数据库操作异常');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorReturn(10001, $e->getMessage());
        }
        DB::commit();
        return $this->successReturn();

    }




    /**
     * 编辑页
     * @param $id
     * @param StepRewardService $stepRewardService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Banner $banner)
    {
        $articleInfo = $banner->find($id);
        return view('admin.banner.edit', [
            'bannerInfo' => $articleInfo,
        ]);
    }

    /**
     * 更新用户信息
     * @param StepRewardRequest $request
     * @param StepRewardService $stepRewardService
     * @return false|string
     * @author:
     * @date: 2019/5/20 19:50
     */
    public function update(Request $request,Banner $banner,UploadsService $uploadsService)
    {
        $params = $request->except(['_token']);
        // 如果传进来了才说要修改图片

        if($request->img_url){
            $path = $uploadsService->uploads_img($request->img_url);
            if(isset($path->original['status_code']) == 'error'){
                return $this->errorReturn(10001,$path->original['data']);
            }

            $params['img_url'] = $path;
            $uploadsService->delete_img($params['old_images']);
            //图片文件字段要unset掉   旧图片也删掉
        }
        unset($params['file']);
        unset($params['old_images']);
        if ($params['id']) {
            DB::beginTransaction();
            try {
                $updateUsers = $banner->where('id',$params['id'])->update($params);
                if (!$updateUsers) {
                    throw new \Exception('编辑出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();
        }
        return $banner->where('id',$params['id'])->update($params) ? $this->successReturn() : $this->errorReturn(10001, '编辑出错');

    }

    /**
     *
     * @param Request $request
     * @param UsersService $usersService
     * @return false|string
     * @author Bryant
     * @date 2020-09-09 19:20
     *
     * s
     */
    public function delete(Request $request,Banner $banner)
    {
        return $banner->where('id',$request->id)->delete() ? $this->successReturn() : $this->errorReturn();
    }



}
