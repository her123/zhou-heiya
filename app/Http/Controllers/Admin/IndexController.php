<?php


namespace App\Http\Controllers\Admin;


use App\Models\Admin\Admin;
use App\Repository\PermissionsRepository;
use App\Repository\AdminHasRolesRepository;
use App\Repository\AdminRepository;
use App\Service\MenuService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class IndexController extends BaseController
{
    public function home()
    {
        return view('admin.index.home');
    }

    /**
     * 后台主页
     * @param Admin $users
     * @param PermissionsRepository $permissionsRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/30 15:34
     */
    public function index(Admin $users,PermissionsRepository $permissionsRepository)
    {
        $adminUserId = Session::get('user_id');
        if(in_array(1,Session::get('role_id'))){
            $permissionsList = $permissionsRepository->getPermissions(['id','icon','name','pid','routes','alias','module','controller','action'],['is_show'=>1]);
        }else{
            $userHasPermissions  = $users->find($adminUserId)->havePermissions->toArray();
            $permissionsList     = $permissionsRepository->userHasPermissions(array_column($userHasPermissions,'permission_id'));
        }
        $permissions         = MenuService::getMenu($permissionsList->toArray());
        return view('admin.index.index',[
            'permissions'=>$permissions
        ]);
    }

    /**
     * 登陆界面
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/6/13 11:25
     */
    public function login()
    {
        return view('admin.index.login');
    }

    /**
     * 登陆操作
     * @param Request $request
     * @param Admin $user
     * @param AdminHasRolesRepository $adminHasRolesRepository
     * @param AdminRepository $adminRepository
     * @return false|string
     * @author:
     * @date: 2019/6/13 11:25
     */
    public function doLogin(Request $request, Admin $user,AdminHasRolesRepository $adminHasRolesRepository,AdminRepository $adminRepository)
    {
        $params = $request->except('_token');
        $userInfo = $user->getUserInfo(['name'=>$params['name']]);
        if(!$userInfo){
            return $this->errorReturn(10001,'没有此账号信息');
        }
        if(Hash::check($params['password'],$userInfo->password)){
            $roleIds = $adminHasRolesRepository->getRoleIds($userInfo->id);
            Session::put('user_id',$userInfo->id);
            Session::put('nick_name',$userInfo->nick_name);
            Session::put('user_name',$userInfo->name);
            Session::put('role_id',array_column($roleIds->toArray(),'role_id'));
            $data['id']             = $userInfo->id;
            $data['last_login_at']  = time();
            $data['last_login_ip']  = $request->getClientIp();
            $adminRepository->update($data);
            return $this->successReturn();
        }else{
            return $this->errorReturn(10001,'用户名或密码错误');
        }
    }

    /**
     * 登出
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @author:
     * @date: 2019/6/13 11:25
     */
    public function loginOut()
    {
        Session::flush();
        return redirect('/admin/login');
    }
}
