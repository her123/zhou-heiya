<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StepRewardRequest;
use App\Models\Admin\Step;
use App\Models\Admin\StepReward;
use App\Service\StepRewardService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StepController extends BaseController
{
    /**
     * 会员步数列表
     * @param Request $request
     * @param Step $step
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/17 15:22
     */
    public function index(Request $request, Step $step)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['mobile', 'like', '%' . $params['keywords'] . '%', 'or'],
            ];
        }
        $usersList = $step->getStep($where);
        return view('admin.step.index', [
            'request' => $request,
            'search' => $params,
            'usersList' => $usersList
        ]);
    }

    /**
     * 步数换好礼列表
     * @param Request $request
     * @param StepReward $stepReward
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/17 15:22
     */
    public function reward(Request $request, StepReward $stepReward)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['mobile', 'like', '%' . $params['keywords'] . '%', 'or'],
            ];
        }
        $usersList = $stepReward->getStep($where);
        foreach ($usersList as $key=>$value){
            $usersList[$key]->month = get_month_name($value->month);
        }

        return view('admin.step.reward', [
            'request' => $request,
            'search' => $params,
            'usersList' => $usersList
        ]);
    }

    /**
     *  添加模板
     * @author:
     * @date: 2019/5/20 19:14
     */
    public function add()
    {
        return view('admin.step.add',[
            "monthList" => get_month_name(),
        ]);
    }

    /**
     * 添加操作
     * @param StepRewardRequest $request
     * @param StepRewardService $stepRewardService
     * @return false|string
     * @author:
     * @date: 2019/5/24 18:00
     */
    public function add_step(StepRewardRequest $request, StepRewardService $stepRewardService)
    {
        $params = $request->except(['_token']);
        DB::beginTransaction();
        try {
            $addUsers = $stepRewardService->store($params);
            if (!$addUsers) {
                throw new \Exception('数据库操作异常');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorReturn(10001, $e->getMessage());
        }
        DB::commit();
        return $this->successReturn();
    }


    /**
     * 编辑页
     * @param $id
     * @param StepRewardService $stepRewardService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, StepRewardService $stepRewardService)
    {
        $stepInfo = $stepRewardService->get($id);
        return view('admin.step.edit', [
            'stepInfo' => $stepInfo,
            "monthList" => get_month_name(),
        ]);
    }

    /**
     * 更新用户信息
     * @param StepRewardRequest $request
     * @param StepRewardService $stepRewardService
     * @return false|string
     * @author:
     * @date: 2019/5/20 19:50
     */
    public function update(StepRewardRequest $request, StepRewardService $stepRewardService)
    {
        $params = $request->except(['_token']);

        if ($params['id']) {
            DB::beginTransaction();
            try {
                $updateUsers = $stepRewardService->update($params);
                if (!$updateUsers) {
                    throw new \Exception('编辑出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();
        }
        return $stepRewardService->update($params) ? $this->successReturn() : $this->errorReturn(10001, '编辑出错');

    }

}
