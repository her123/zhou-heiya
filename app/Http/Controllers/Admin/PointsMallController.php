<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PointsMallRequest;
use App\Models\Admin\PointsMall;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use App\Service\PointsMallService;
use App\Service\UploadsService;

class PointsMallController extends BaseController
{
    /**
     * 管理员列表
     * @param PointsMall $mall
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/17 15:22
     */
    public function index(Request $request, PointsMall $mall)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['title', 'like', '%' . $params['keywords'] . '%', 'or'],
            ];
        }
        $mallList = $mall->getMall($where);
        return view('admin.mall.index', [
            'request' => $request,
            'search' => $params,
            'mallList' => $mallList
        ]);
    }

    /**
     *  添加用户模板
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/20 19:14
     */
    public function add()
    {
        return view('admin.mall.add');
    }

    /**
     * 添加用户操作
     * @param PointsMallRequest $request
     * @param PointsMallService $mallService
     * @param UploadsService $uploadsService
     * @return false|string
     * @throws \Exception
     * @author:
     * @date: 2019/5/24 18:00
     */
    public function add_mall(PointsMallRequest $request, PointsMallService $mallService,UploadsService $uploadsService)
    {
        $params = $request->except(['_token']);

        // 必须上传积分商品图片
        if(!$request->file){
            return $this->errorReturn(10001, '请选择图片上传');
        }

        $path = $uploadsService->uploads_img($request->file);
        if(isset($path->original['status_code']) == 'error'){
            return $this->errorReturn(10001,$path->original['data']);
        }

        $params['images'] = $path;
        //图片文件字段要unset掉
        unset($params['file']);

        DB::beginTransaction();
        try {
            $addUsers = $mallService->store($params);
            if (!$addUsers) {
                throw new \Exception('数据库操作异常');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorReturn(10001, $e->getMessage());
        }
        DB::commit();
        return $this->successReturn();
    }

    /**
     * 积分商品详情
     * @param $id
     * @param PointsMallService $mallService 积分商品详情
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/20 19:15
     */
    public function edit($id, PointsMallService $mallService)
    {
        $mallInfo = $mallService->get($id);
        return view('admin.mall.edit', [
            'mallInfo' => $mallInfo
        ]);
    }

    /**
     * 更新银行卡信息
     * @param PointsMallRequest $request
     * @param PointsMallService $mallService
     * @param UploadsService $uploadsService
     * @return false|string
     * @author:
     * @date: 2019/5/20 19:50
     */
    public function update(Request $request, PointsMallService $mallService,UploadsService $uploadsService)
    {
        $params = $request->except(['_token']);
        // 如果传进来了才说要修改图片
        if($request->file){
            $path = $uploadsService->uploads_img($request->file);
            if(isset($path->original['status_code']) == 'error'){
                return $this->errorReturn(10001,$path->original['data']);
            }

            $params['images'] = $path;
            $uploadsService->delete_img($params['old_images']);
            //图片文件字段要unset掉   旧图片也删掉
        }
        unset($params['file']);
        unset($params['old_images']);

        if ($params['id'] != 1) {
            DB::beginTransaction();
            try {
                $updateBank = $mallService->update($params);
                if (!$updateBank) {
                    throw new \Exception('操作编辑出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();
        }
        return $mallService->update($params) ? $this->successReturn() : $this->errorReturn(10001, '银行卡信息编辑出错');
    }

    /**
     * 删除银行卡信息
     * @param Request $request
     * @param PointsMallService $mallService
     * @return false|string
     * @author:
     * @date: 2019/5/20 20:27
     */
    public function delete(Request $request, PointsMallService $mallService)
    {
        return $mallService->delete($request->id) ? $this->successReturn() : $this->errorReturn();
    }

    /**
     * 银行卡禁用
     * @param Request $request
     * @param PointsMallService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/22 15:42
     */
    public function pullBack(Request $request, PointsMallService $mallService)
    {
        $userStatus = 1;
        if ($request->post('type') == 1) {
            $userStatus = 0;
        }

        return $mallService->pullBlack($request->id, $userStatus) ? $this->successReturn() : $this->errorReturn(10001, '操作异常');
    }


    /**
     * 获取菜单下拉列表
     * @param $userList
     * @param $id
     * @return string
     * @author:
     * @date: 2019/5/29 17:28
     */
    public function UserSelect($userList,$id)
    {
        return PointsMallService::showSelect($userList,$id);
    }
}
