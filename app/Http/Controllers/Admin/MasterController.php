<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StepRewardRequest;
use App\Models\Admin\Step;
use App\Models\Admin\StepReward;
use App\Models\Master;
use App\Service\HttpService;
use App\Service\StepRewardService;
use App\Service\UsersService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterController extends BaseController
{
    /**
     *
     * @param Request $request
     * @param Step $step
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Bryant
     * @date 2020-09-09 15:22
     *
     * 优惠券的列表
     */
    public function index(Request $request,Master $master)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['mobile', 'like', '%' . $params['keywords'] . '%', 'or'],
            ];
        }
        $usersList = $master->where($where)->orderby('id', 'desc')->paginate(15);
        return view('admin.master.index', [
            'request' => $request,
            'search' => $params,
            'usersList' => $usersList
        ]);
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author Bryant
     * @date 2020-09-09 16:12
     *
     * 添加优惠券
     */
    public function add()
    {
        return view('admin.coupon.add');
    }


    /**
     *
     * @author Bryant
     * @date 2020-09-09 16:13
     *
     * 添加
     */
    public function store(Request $request,Master $master)
    {
        $params = $request->except(['_token']);
        DB::beginTransaction();
        try {
            $addUsers = $coupon->insert($params);
            if (!$addUsers) {
                throw new \Exception('数据库操作异常');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorReturn(10001, $e->getMessage());
        }
        DB::commit();
        return $this->successReturn();

    }




    /**
     * 编辑页
     * @param $id
     * @param StepRewardService $stepRewardService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Master $master,HttpService $httpService)
    {
        $couponInfo = $master->find($id);
        $mobile = $couponInfo->mobile;

        $queryStr['queryStr'] =$mobile; // 参数根据手机号

        $queryType['queryType'] = 2;  // 类型根据手机号
        // 查询用户的昵称 等级
        $user = $this->getUser($httpService,$queryStr,$queryType);
        $couponInfo['levelText'] = $user['levelText'];
        $couponInfo['name'] = $user['name'];
        return view('admin.master.edit', [
            'couponInfo' => $couponInfo,
        ]);
    }

    /**
     * 更新用户信息
     * @param StepRewardRequest $request
     * @param StepRewardService $stepRewardService
     * @return false|string
     * @author:
     * @date: 2019/5/20 19:50
     */
    public function update(Request $request,Master $master)
    {
        $params = $request->except(['_token']);

        if ($params['id']) {
            DB::beginTransaction();
            try {
                $updateUsers = $master->where('id',$params['id'])->update($params);
                if (!$updateUsers) {
                    throw new \Exception('编辑出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();
        }
        return $master->where('id',$params['id'])->update($params) ? $this->successReturn() : $this->errorReturn(10001, '编辑出错');

    }

    /**
     *
     * @param Request $request
     * @param UsersService $usersService
     * @return false|string
     * @author Bryant
     * @date 2020-09-09 19:20
     *
     * s
     */
    public function delete(Request $request,Master $master)
    {
        return $master->where('id',$request->id)->delete() ? $this->successReturn() : $this->errorReturn();
    }



}
