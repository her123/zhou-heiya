<?php

namespace App\Http\Controllers\Admin;

use App\Repository\RolesRepository;
use App\Repository\AdminRepository;
use App\Http\Requests\UsersRequest;
use App\Models\Admin\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use App\Service\UsersService;

class UsersController extends BaseController
{
    /**
     * 管理员列表
     * @param Users $users
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/17 15:22
     */
    public function index(Request $request, Users $users)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['name', 'like', '%' . $params['keywords'] . '%', 'or'],
                ['mobile', 'like', '%' . $params['keywords'] . '%', 'or'],
                ['email', 'like', '%' . $params['keywords'] . '%', 'or']
            ];
        }
        $usersList = $users->getUsers($where);
        return view('admin.users.index', [
            'request' => $request,
            'search' => $params,
            'usersList' => $usersList
        ]);
    }

    /**
     *  添加用户模板
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/20 19:14
     */
    public function add(RolesRepository $rolesRepository)
    {
        return view('admin.users.add');
    }

    /**
     * 添加用户操作
     * @param UsersRequest $request
     * @param AdminRepository $users
     * @return false|string
     * @throws \Exception
     * @author:
     * @date: 2019/5/24 18:00
     */
    public function add_users(UsersRequest $request, UsersService $usersService)
    {
        $params = $request->except(['_token', 'password_confirmation','withdrawal_password_confirmation']);
        $params['password']     = \Hash::make($params['password']);
        $params['withdrawal_password']     = \Hash::make($params['withdrawal_password']);
        DB::beginTransaction();
        try {
            $addUsers = $usersService->store($params);
            if (!$addUsers) {
                throw new \Exception('数据库操作异常');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorReturn(10001, $e->getMessage());
        }
        DB::commit();
        return $this->successReturn();
    }

    /**
     * 用户详情
     * @param $id 用户id
     * @param UsersService $usersService 用户信息查找
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/20 19:15
     */
    public function edit($id, UsersService $usersService)
    {
        $userInfo = $usersService->get($id);
        return view('admin.users.edit', [
            'userInfo' => $userInfo
        ]);
    }

    /**
     * 更新用户信息
     * @param UsersRequest $request
     * @param AdminRepository $usersRepository
     * @return false|string
     * @author:
     * @date: 2019/5/20 19:50
     */
    public function update(UsersRequest $request, UsersService $usersService)
    {
        $params = $request->except(['_token', 'password_confirmation','withdrawal_password_confirmation']);

        // 账号密码
        if(!empty($params['password'])){
            $params['password'] = \Hash::make($params['password']);
        }else{
            unset($params['password']);  // 不传密码则unset掉不修改
        }
        // 提现密码
        if(!empty($params['withdrawal_password'])){
            $params['withdrawal_password'] = \Hash::make($params['withdrawal_password']);
        }else{
            unset($params['withdrawal_password']);  // 不传确认密码则unset掉不修改
        }

        if ($params['id'] != 1) {
            DB::beginTransaction();
            try {
                $updateUsers = $usersService->update($params);
                if (!$updateUsers) {
                    throw new \Exception('用户信息编辑出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();
        }
        return $usersService->update($params) ? $this->successReturn() : $this->errorReturn(10001, '用户信息编辑出错');

    }

    /**
     * 删除用户信息
     * @param Request $request
     * @param AdminRepository $usersRepository
     * @return false|string
     * @author:
     * @date: 2019/5/20 20:27
     */
    public function delete(Request $request, UsersService $usersService)
    {
        if ($request->id == 1) {
            return $this->errorReturn(10001, '超级管理员不得删除');
        };
        return $usersService->delete($request->id) ? $this->successReturn() : $this->errorReturn();
    }

    /**
     * 用户拉黑
     * @param Request $request
     * @param AdminRepository $usersRepository
     * @return false|string
     * @author:
     * @date: 2019/5/22 15:42
     */
    public function pullBack(Request $request, UsersService $usersService)
    {
        $userStatus = 1;
        if ($request->post('type') == 1) {
            $userStatus = 0;
        }
        if ($request->id == 1) {
            return $this->errorReturn(10001, '超级管理员不得删除');
        };
        return $usersService->pullBlack($request->id, $userStatus) ? $this->successReturn() : $this->errorReturn(10001, '操作异常');
    }

    public function editHeadPortrait()
    {
        return view('admin.edit_head_portrait');
    }
}
