<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BankRequest;
use App\Models\ActivityRules;
use App\Models\Admin\Users;
use App\Repository\RolesRepository;
use App\Repository\AdminRepository;
use App\Http\Requests\UsersRequest;
use App\Models\Admin\Bank;
use App\Service\UploadsService;
use App\Service\UsersService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;
use App\Service\BankService;

class ActivityRulesController extends BaseController
{
    /**
     * 管理员列表
     * @param Bank $users
     * @return Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/17 15:22
     */
    public function index(Request $request, ActivityRules $activityRules)
    {
        $params = $request->except('_token');
        $where = [];
        if (isset($params['keywords'])) {
            $where = [
                ['name', 'like', '%' . $params['keywords'] . '%', 'or'],

            ];
        }
        $activityList = $activityRules->where($where)->orderBy('id','desc')->paginate();
        return view('admin.activity.index', [
            'request' => $request,
            'search' => $params,
            'activityList' => $activityList
        ]);
    }

    /**
     *  添加用户模板
     * @param UsersService $usersService
     * @return Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/20 19:14
     */
    public function add(UsersService $usersService)
    {
        return view('admin.activity.add',[
            'users' => $usersService->allUsers(),
        ]);
    }

    /**
     * 添加用户操作
     * @param BankRequest $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/24 18:00
     */
    public function store(Request $request,ActivityRules $activityRules,UploadsService $uploadsService)
    {
        $params = $request->except(['_token']);

        // 必须上传积分商品图片
        if(!$request->banner_img){
            return $this->errorReturn(10001, '请选择图片上传');
        }

        $path = $uploadsService->uploads_img($request->banner_img);
        if(isset($path->original['status_code']) == 'error'){
            return $this->errorReturn(10001,$path->original['data']);
        }

        $params['banner_img'] = $path;
        //图片文件字段要unset掉
        unset($params['file']);
        DB::beginTransaction();
        try {
            $addUsers = $activityRules->insert($params);
            if (!$addUsers) {
                throw new \Exception('数据库操作异常');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->errorReturn(10001, $e->getMessage());
        }
        DB::commit();
        return $this->successReturn();
    }

    /**
     * 用户详情
     * @param $id
     * @param BankService $bankService
     * @return Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/20 19:15
     */
    public function edit($id, ActivityRules $activityRules,UsersService $usersService)
    {
        $userInfo = $activityRules->find($id);
        return view('admin.activity.edit', [
            'active' => $userInfo,
            'users' => $usersService->allUsers(),
        ]);
    }

    /**
     * 更新用户信息
     * @param BankRequest $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/20 19:50
     */
    public function update(Request $request,ActivityRules $activityRules,UploadsService $uploadsService)
    {
        $params = $request->except(['_token']);
        // 如果传进来了才说要修改图片
        if($request->banner_img){
            $path = $uploadsService->uploads_img($request->banner_img);
            if(isset($path->original['status_code']) == 'error'){
                return $this->errorReturn(10001,$path->original['data']);
            }

            $params['banner_img'] = $path;
            $uploadsService->delete_img($params['old_images']);
            //图片文件字段要unset掉   旧图片也删掉
        }
        unset($params['file']);
        unset($params['old_images']);

        if ($params['id']) {
            DB::beginTransaction();
            try {
                $updateUsers = $activityRules->where('id',$params['id'])->update($params);
                if (!$updateUsers) {
                    throw new \Exception('活动信息编辑出错');
                }
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->errorReturn(10001, $e->getMessage());
            }
            DB::commit();
            return $this->successReturn();
        }
        return $this->errorReturn(10001, '编辑出错');

    }

    /**
     * 删除用户信息
     * @param Request $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/20 20:27
     */
    public function delete(Request $request,ActivityRules $activityRules)
    {
        return $activityRules->where('id',$request->id)->delete() ? $this->successReturn() : $this->errorReturn();
    }

    /**
     * 用户拉黑
     * @param Request $request
     * @param BankService $bankService
     * @return false|string
     * @author:
     * @date: 2019/5/22 15:42
     */
    public function pullBack(Request $request, BankService $bankService)
    {
        $userStatus = 1;
        if ($request->post('type') == 1) {
            $userStatus = 0;
        }

        return $bankService->pullBlack($request->id, $userStatus) ? $this->successReturn() : $this->errorReturn(10001, '操作异常');
    }

}
