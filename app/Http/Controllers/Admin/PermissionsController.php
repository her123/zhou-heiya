<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PermissionsRequest;
use App\Http\Requests\Request;
use App\Repository\PermissionsRepository;
use App\Service\MenuService;

class PermissionsController extends BaseController
{
    /**
     * 菜单列表
     * @param PermissionsRepository $permissionsRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(PermissionsRepository $permissionsRepository)
    {
        $permissionsData = $permissionsRepository->getPermissions(['id','name','pid','module','controller','action','alias','is_show','created_at'])->toArray();

        foreach ($permissionsData as $key => $value){
            $permissionsData[$key]['routes'] = $value['module'] . '/' . $value['controller'] . '/' . $value['action'];
        }

        return view('admin.permissions.index',[
            'data' => json_encode($permissionsData)
        ]);
    }

    /**
     * 添加菜单模板
     * @param PermissionsRepository $permissionsRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author:
     * @date: 2019/5/29 17:23
     */
    public function add($id = '',PermissionsRepository $permissionsRepository)
    {
        $permissions        =  $permissionsRepository->getPermissions(['id','name','pid']);
        return view('admin.permissions.add',[
            'permissions'   => $this->permissionsSelect($permissions,$id ?? 0)
        ]);
    }

    /**
     * 添加菜单
     * @param PermissionsRequest $request
     * @param PermissionsRepository $permissionsRepository
     * @author:
     * @date: 2019/5/29 17:22
     */
    public function addPermissions(PermissionsRequest $request,PermissionsRepository $permissionsRepository)
    {
       $params = $request->except('_token');
       return $permissionsRepository->add($params) ? $this->successReturn() : $this->errorReturn();
    }

    /**
     * 编辑菜单
     * @param $id
     * @param PermissionsRepository $permissionsRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,$pid,PermissionsRepository $permissionsRepository)
    {
        $permissions        =  $permissionsRepository->getPermissions(['id','name','pid']);
        return view('admin.permissions.edit',[
           'permission' => $permissionsRepository->get($id),
           'permissions' => $this->permissionsSelect($permissions,$pid)
        ]);
    }

    /**
     * 保存修改菜单
     * @param PermissionsRequest $request
     * @param PermissionsRepository $permissionsRepository
     * @return false|string
     * @author:
     * @date: 2019/5/29 17:28
     */
    public function save(PermissionsRequest $request,PermissionsRepository $permissionsRepository)
    {
        $params = $request->except('_token');
        return $permissionsRepository->save($params) ? $this->successReturn() : $this->errorReturn();
    }

    public function delete(Request $request,PermissionsRepository $permissionsRepository)
    {
        if(count($permissionsRepository->children($request->id)) > 0){
            return $this->errorReturn(10001,'该菜单下含有子菜单，禁止删除');
        }
        return $permissionsRepository->delete($request->id) ? $this->successReturn() : $this->errorReturn(10001,'菜单删除失败');
    }
    /**
     * 获取菜单下拉列表
     * @param $permissions
     * @param int $id
     * @param int $pid
     * @param int $level
     * @return string
     * @author:
     * @date: 2019/5/29 17:28
     */
    public function permissionsSelect($permissions,$id = 0,$pid = 0,$level = 0)
    {
        $menus = MenuService::categoryTree($permissions->toArray(),$pid,$level);
        return MenuService::showSelect($menus,$id);
    }


}
