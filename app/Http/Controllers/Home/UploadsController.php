<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\UploadsService;

class UploadsController extends Controller
{
    // 前端接口上传图片专用
    public function uploads_img(Request $request,UploadsService $uploadsService){
        $uploadFile = !empty($request->file('uploadFile')) ? $request->file('uploadFile') : [];
        if (!empty($uploadFile)) {
            $path = $uploadsService->uploads_img($uploadFile);

            if(isset($path->original['status_code']) == 'error'){
                return tips(-1,'error',$path->original['data']);
            }

            return tips(1,'success',$path);
        }
    }
}
