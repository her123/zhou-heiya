<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Service\HttpService;
use App\Service\SmsService;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    //
    public function sendSms(Request $request, SmsService $service,HttpService $httpService)
    {
        $mobile = request('mobile');
        if (!checkMobile($mobile)) {
            return tips(-1, 'error', '手机号码格式错误');
        }

        $msg='【测试】这是一条测试短信，给我放尊重点！';
        return $service->sendSMS($httpService ,$mobile,$msg);
    }
}
