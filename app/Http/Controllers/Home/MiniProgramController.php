<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Service\CacheService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use App\Service\MiniProgramService;
use App\Service\HttpService;
use App\Traits\JsonResponse;
use Illuminate\Support\Facades\Log;

class MiniProgramController extends Controller
{
    use JsonResponse;

    /**
     * @param MiniProgramService $miniProgramService
     * @param HttpService $httpService
     * @return false|\Illuminate\Http\JsonResponse|string
     */
//    public function getMiniProgramMobile(MiniProgramService $miniProgramService,HttpService $httpService){
//        $code = request(['code']);
//        if(!$code){
//            return tips(-1,'error','参数错误');
//        }
//
//        try {
//            $data = $miniProgramService->getOpenID($code['code'],$httpService);
//        } catch (\Exception $e) {
//            return $this->errorReturn(10001, $e->getMessage());
//        }
//
//        $iv = request(['iv']);
//        $encryptedData = request(['encryptedData']);
//        Log::debug('getMiniProgramMobile------：'.json_encode(['iv'=>$iv,'encryptedData'=>$encryptedData,'code'=>$code]));
//
//        $openid = isset($data['openid']) ? $data['openid'] : '';
//        if (!$openid) {
//            return tips(-1,'error', '获取openid失败...');
//        }
//        $session_key = $data['session_key'];    // 解密手机号码需要
//
//        try {
//            $errCode = $miniProgramService->decryptData( $encryptedData['encryptedData'], $iv['iv'],$session_key, $decryptData);
//        } catch (\Exception $e) {
//            return tips(-1,'error',$e->getMessage());
//        }
//        $decryptData = json_decode($decryptData,true);
//        if ($errCode == 0) {
//            return tips(1,'success',$encryptedData['phoneNumber']);
//        }else{
//            return tips(-1,'error',$errCode);
//        }
//    }

    public function test(CacheService $cacheService){
//        $cacheService::setList('testList','test2');

        //$list = $cacheService::getList('testList');
        $arr = ['test2','test6'];
        foreach ($arr as $key=>$value){
//            $new = array_search($value,$list);
//            unset($list[$new]);
            $cacheService::delList('testList',1,$value);
        }

        return $cacheService::getList('testList');

    }

    public function is_json($str){
        $flag = is_null(json_decode($str));
        return $flag===true?false:true;
    }


    public function get()
    {
        /* header('Content-Type:text/html;charset=utf-8');

       $url = "http://wdqas01.zhouheiya.cn:50000/dir/wsdl?p=ic/27fa8ddb5d5230ae920d01a4d7ea7ca8";
       $client = new \SoapClient($url,array('encoding'=>"gb2312",'location'=>'http://WDQAS01.zhouheiya.cn'));
       dd($client->__getFunctions());
       $parms = array(
           'PARTNER' => "hujun",
       );*/


        /*  //header('Content-Type:text/html;charset=utf-8');f

          $ws = "http://wdqas01.zhouheiya.cn:50000/dir/wsdl?oid=080ec8e04acb320f810685567fc9d835";
          //$ws = "http://wdqas01.zhouheiya.cn:50000/dir/wsdl?p=ic/27fa8ddb5d5230ae920d01a4d7ea7ca8";

          //$ws = "http://ws.webxml.com.cn/WebServices/WeatherWS.asmx?wsdl";//webservice服务的地址
          $client = new SoapClient($ws);
          $client ->debugLastSoapRequest();
          dd($client);
          $result=$client->getWeather(array('theCityCode'=>'广州','theUserID'=>''));//查询中国深圳的天气，返回的是一个结构体print_r($result);
          dd($result);
          phpinfo();*/

        // 用户id
        //$memberId = $this->userInfo();
    }

}
