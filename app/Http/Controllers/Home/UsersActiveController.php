<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Admin\UsersActive;
use App\Models\Admin\UsersActiveAssociation;
use App\Service\CacheService;
use App\Service\RewardLogsService;
use App\Service\UsersActiveAssociationService;
use App\Service\UsersService;
use Illuminate\Http\Request;
use App\Service\UsersActiveService;
use Illuminate\Support\Facades\DB;

class UsersActiveController extends Controller
{
    /**
     * @param UsersActiveService $activeService
     * @param UsersActiveAssociationService $activeAssociationService
     * @param UsersActiveAssociation $activeAssociation
     * @return \Illuminate\Http\JsonResponse
     */
    public function active(UsersActiveService $activeService,UsersActiveAssociationService $activeAssociationService,UsersActiveAssociation $activeAssociation){

//        $user_id = isset(auth()->user('api')->id)?auth()->user('api')->id:false;
//        if(!$user_id){
//            return tips(-1,'error','');
//        }

        $active_id = request('active_id');
        $mobile = request('mobile');
        if(!$mobile){
            return tips(-1,'error','参数错误');
        }

        $active = $activeService->get($active_id);
        if(!$active){
            return tips(-1,'error','暂无活动');
        }
        $active->path = config('app.img_url');
        $active->is_active = -1; // 未参加活动
        $list = $activeAssociation->getActiveUserList();

        $active->avatar_list = $list;
        $count = $activeAssociationService->getCount(['active_id'=>$active->id]);
        $active->count = $count; // 共多少人参加

        if($mobile){
            $log = $activeAssociationService->getActiveInfo(['active_id'=>$active->id,'mobile'=>$mobile]);
            if($log){
                $active->is_active = 1; // 已参加活动
            }
        }

        $active->reward_level = \GuzzleHttp\json_decode($active->reward_level);
        $active->start_time = date('Y-m-d',strtotime($active->start_time));
        $active->end_time = date('Y-m-d',strtotime($active->end_time));
        $active->active_time = date('m月d日',strtotime($active->start_time)).'-'.date('m月d日',strtotime($active->end_time));

        if($active){
            return tips(1,'success',$active);
        }
        return tips(0,'error',[]);
    }

    public function activeTest(UsersActiveService $activeService,UsersActiveAssociationService $activeAssociationService,UsersActiveAssociation $activeAssociation){

//        $user_id = isset(auth()->user('api')->id)?auth()->user('api')->id:false;
//        if(!$user_id){
//            return tips(-1,'error','');
//        }

        $active_id = request('active_id');
        if(!$active_id){
            return tips(-1,'error','参数错误');
        }

        $active = $activeService->get($active_id);
        if(!$active){
            return tips(-1,'error','暂无活动');
        }

        $active->path = config('app.img_url');
        $active->is_active = -1; // 未参加活动
        $list = $activeAssociation->getActiveUserList();
        $active->avatar_list = $list;
        $count = $activeAssociationService->getCount(['active_id'=>$active->id]);
        $active->count = $count; // 共多少人参加
//        if($user_id){
//            $log = $activeAssociationService->getActiveInfo(['active_id'=>$active->id,'users_id'=>$user_id]);
//            if($log){
//                $active->is_active = 1; // 已参加活动
//            }
//        }

        $active->reward_level = \GuzzleHttp\json_decode($active->reward_level);
        $active->start_time = date('Y-m-d',strtotime($active->start_time));
        $active->end_time = date('Y-m-d',strtotime($active->end_time));
        $active->active_time = date('m月d日',strtotime($active->start_time)).'-'.date('m月d日',strtotime($active->end_time));

        if($active){
            return tips(1,'success',$active);
        }
        return tips(0,'error',[]);
    }

    /**
     * 会员参与活动
     * @param UsersActiveAssociationService $activeAssociationService
     * @param UsersActiveService $activeService
     * @param UsersService $usersService
     * @param CacheService $cacheService
     * @return \Illuminate\Http\JsonResponse
     */
    public function addActive(UsersActiveAssociationService $activeAssociationService,UsersActiveService $activeService,UsersService $usersService,CacheService $cacheService){

//        $user_id = isset(auth()->user('api')->id)?auth()->user('api')->id:false;
//        if(!$user_id){
//            return tips(-1,'error','');
//        }

        $active_id = request('active_id');
        $mobile = request('mobile');
        $active_images = request('active_images');
        $channel = request('channel');
        $avatar_url = request('avatar_url');
        $store = request('store');

        if(!$active_id || !$active_images || !$channel || !$mobile){
            return tips(-1,'error','参数错误');
        }

        $active = $activeAssociationService->getActiveInfo(['mobile'=>$mobile,'active_id'=>$active_id]);
        if($active){
            return tips(-1,'error','请勿重复参与活动');
        }

        $active = $activeService->get($active_id);
        if(!$active){
            return tips(-1,'error','活动不在参与时间');
        }

        $params['mobile'] = $mobile;
        $params['active_id'] = $active_id;
        $params['active_images'] = $active_images;
        $params['channel'] = $channel;
        $params['store'] = $store;
        $params['avatar'] = $avatar_url;
        //$update['id'] = $user_id;

        DB::beginTransaction();
        try {
            $add = $activeAssociationService->store($params);
            if (!$add) {
                throw new \Exception('参与活动出错');
            }else{
                $cacheService::setList('participate_active_'.$active_id,$mobile);  // 参与活动存到 reids
                //$usersService->update($update); //更新会员头像
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return tips(-1,'error', $e->getMessage());
        }
        DB::commit();
        return tips(1,'success', '参与成功');
    }

    /**
     * 我参加的活动列表
     * @param UsersActiveAssociation $activeAssociation
     * @return \Illuminate\Http\JsonResponse
     */
    public function activeList(UsersActiveAssociation $activeAssociation){

        $user_id = isset(auth()->user('api')->id)?auth()->user('api')->id:false;
        if(!$user_id){
            return tips(-1,'error','');
        }

        $activeList = $activeAssociation->getUsers(['users_id'=>$user_id]);
        foreach ($activeList as $key=>$value){
            $activeList[$key]->start_time = date('Y-m-d',strtotime($value->start_time));
            $activeList[$key]->end_time = date('Y-m-d',strtotime($value->end_time));
        }
        return tips(1,'success',$activeList);
    }
}
