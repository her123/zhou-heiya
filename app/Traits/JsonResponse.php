<?php
namespace App\Traits;

trait JsonResponse
{
    /**
     * 成功返回值
     * @param int $code
     * @param string $message
     * @param array $info
     * @return false|string
     * @author:
     * @date: 2019/4/28 15:57
     */
    public function successReturn($code = 10000,$message = '成功',$info = []){
        return json_encode(
            [
                'code'      =>$code,
                'message'   =>$message,
                'info'      =>$info

            ]
        );
    }

    /**
     * 失败返回值
     * @param int $code
     * @param string $message
     * @param array $info
     * @return false|string
     * @author:
     * @date: 2019/4/28 15:57
     */
    public function errorReturn($code = 10001,$message = '失败',$info = []){
        return json_encode(
            [
                'code'      =>$code,
                'message'   =>$message,
                'info'      =>$info

            ]
        );
    }
}