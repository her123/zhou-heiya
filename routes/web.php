<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// 后台路由
Route::namespace('Admin')->group(function () {
    // 在 "App\Http\Controllers\Admin" 命名空间下的控制器
    //中间件  判断有没有登陆
    Route::group(['middleware' => 'check.login'], function() {
        //内部为，不想让未登录用户进的路由
        Route::get('/admin','IndexController@index');
        Route::get('/admin/home','IndexController@home')->name('admin.home');
        Route::group(['middleware' => 'auth.verify'], function() {
            //管理员管理
            Route::match(['get', 'post'], '/admin/admin', 'AdminController@index')->name('admin.index');
            Route::match(['get', 'post'], '/admin/admin/add', 'AdminController@add')->name('admin.add');
            Route::post('/admin/admin/do_add', 'AdminController@doAdd')->name('admin.do_add');
            Route::get('/admin/admin/edit/{id}', 'AdminController@edit')->name('admin.edit');
            Route::post('/admin/admin/update', 'AdminController@update')->name('admin.update');
            Route::post('/admin/admin/delete', 'AdminController@delete')->name('admin.delete');
            Route::post('/admin/admin/pull_back', 'AdminController@pullBack')->name('admin.pull_back');
            Route::match(['get', 'post'], '/admin/admin/edit_admin', 'AdminController@editAdmin')->name('admin.edit_admin');
            //角色管理
            Route::match(['get', 'post'], '/admin/roles', 'RolesController@index')->name('admin.roles');  //角色列表
            Route::get('/admin/roles/add', 'RolesController@add')->name('roles.add'); //角色添加模板
            Route::post('/admin/roles/add_roles', 'RolesController@addRoles')->name('roles.add_roles'); // 角色添加操作
            //获取角色权限列表
            Route::match(['get', 'post'], '/admin/roles/getPermissions', 'RolesController@getPermissions')->name('roles.getPermissions');
            Route::get('/admin/roles/permissions', 'RolesController@permissions')->name('roles.permissions');//  权限设置列表
            Route::post('/admin/roles/permissions_add', 'RolesController@permissionsAdd')->name('roles.permissions_add'); //权限设置操作
            Route::get('/admin/roles/edit/', 'RolesController@edit')->name('roles.edit'); //权限设置操作
            Route::post('/admin/roles/save', 'RolesController@save')->name('roles.save'); //权限设置操作
            Route::post('/admin/roles/delete', 'RolesController@delete')->name('roles.delete'); //权限设置操作

            //菜单管理
            Route::get('/admin/permissions/index', 'PermissionsController@index')->name('permissions.index');
            Route::get('/admin/permissions/lists', 'PermissionsController@lists')->name('permissions.lists');
            Route::get('/admin/permissions/add/{id?}', 'PermissionsController@add')->name('permissions.add');
            Route::get('/admin/permissions/edit/{id}/pid/{pid}', 'PermissionsController@edit')->name('permissions.edit');
            Route::post('/admin/permissions/save', 'PermissionsController@save')->name('permissions.save');
            Route::post('/admin/permissions/delete', 'PermissionsController@delete')->name('permissions.delete');
            Route::post('/admin/permission/add_permissions', 'PermissionsController@addPermissions')->name('permissions.add_permissions');

            // 会员用户列表
            Route::match(['get', 'post'], '/admin/users/index', 'UsersController@index')->name('users.index');  //角色列表
            Route::get('/admin/users/add', 'UsersController@add')->name('users.add'); //用户添加
            Route::post('/admin/users/add_users', 'UsersController@add_users')->name('users.add_users');//用户添加
            Route::get('/admin/users/edit/{id}', 'UsersController@edit')->name('users.edit'); //用户编辑
            Route::post('/admin/users/update', 'UsersController@update')->name('users.update');//用户
            Route::post('/admin/users/delete', 'UsersController@delete')->name('users.delete'); //权限设置操作
            Route::post('/admin/users/pull_back', 'UsersController@pullBack')->name('users.pull_back');

            // 银行卡列表
            Route::match(['get', 'post'], '/admin/bank/index', 'BankController@index')->name('bank.index');  //银行卡信息列表
            Route::get('/admin/bank/add', 'BankController@add')->name('bank.add'); //银行卡信息添加
            Route::post('/admin/bank/add_bank', 'BankController@add_bank')->name('bank.add_bank');//银行卡信息添加操作
            Route::get('/admin/bank/edit/{id}', 'BankController@edit')->name('bank.edit'); //银行卡信息编辑
            Route::post('/admin/bank/update', 'BankController@update')->name('bank.update');//银行卡信息编辑操作
            Route::post('/admin/bank/delete', 'BankController@delete')->name('bank.delete'); //银行卡信息删除
            Route::post('/admin/bank/pull_back', 'BankController@pullBack')->name('bank.pull_back');//银行卡信息禁用启用

            // 积分商品列表
            Route::match(['get', 'post'], '/admin/mall/index', 'PointsMallController@index')->name('mall.index');  //角色列表
            Route::get('/admin/mall/add', 'PointsMallController@add')->name('mall.add'); //用户添加
            Route::post('/admin/mall/add_mall', 'PointsMallController@add_mall')->name('mall.add_mall');//用户添加
            Route::get('/admin/mall/edit/{id}', 'PointsMallController@edit')->name('mall.edit'); //用户编辑
            Route::post('/admin/mall/update', 'PointsMallController@update')->name('mall.update');//用户
            Route::post('/admin/mall/delete', 'PointsMallController@delete')->name('mall.delete'); //权限设置操作
            Route::post('/admin/mall/pull_back', 'PointsMallController@pullBack')->name('mall.pull_back');
            // 积分订单列表
            Route::match(['get', 'post'], '/admin/mallorder/index', 'PointsMallOrderController@index')->name('order.index');

            // 活动列表
            Route::match(['get', 'post'], '/admin/active/index', 'UsersActiveController@index')->name('active.index');  //角色列表
            Route::get('/admin/active/add', 'UsersActiveController@add')->name('active.add'); //用户添加
            Route::post('/admin/active/add_active', 'UsersActiveController@add_active')->name('active.add_active');//添加操作
            Route::get('/admin/active/edit/{id}', 'UsersActiveController@edit')->name('active.edit'); //编辑
            Route::post('/admin/active/update', 'UsersActiveController@update')->name('active.update');//更新
            Route::post('/admin/active/delete', 'UsersActiveController@delete')->name('active.delete'); //删除
            Route::match(['get', 'post'], '/admin/active/users_lists', 'UsersActiveController@users_lists')->name('active.users_lists');  //角色列表
            Route::post('/admin/active/users_active_delete', 'UsersActiveController@users_active_delete')->name('active.users_active_delete'); //删除
            Route::get('/admin/active/add_users', 'UsersActiveController@add_users')->name('active.add_users'); //添加活动会员
            Route::post('/admin/active/add_users_active', 'UsersActiveController@add_users_active')->name('active.add_users_active'); //添加活动会员操作
            Route::post('/admin/active/set_reward', 'UsersActiveController@set_reward')->name('active.set_reward'); //设置中奖会员
            Route::match(['get', 'post'], '/admin/active/reward_lists', 'UsersActiveController@reward_lists')->name('active.reward_lists');  //活动中奖列表
            Route::get('/admin/active/active_qrcode', 'UsersActiveController@active_qrcode')->name('active.active_qrcode'); // 生成活动二维码
            Route::post('/admin/active/create_qrcode', 'UsersActiveController@create_qrcode')->name('active.create_qrcode'); // 生成活动二维码操作
            Route::post('/admin/active/reward', 'UsersActiveController@reward')->name('active.reward'); // 生成活动二维码操作


            // 步数列表
            Route::match(['get', 'post'], '/admin/step/index', 'StepController@index')->name('step.index');
            Route::match(['get', 'post'], '/admin/step/reward', 'StepController@reward')->name('step.reward');
            Route::get('/admin/step/add', 'StepController@add')->name('step.add');
            Route::post('/admin/step/add_step', 'StepController@add_step')->name('step.add_step');
            Route::get('/admin/step/edit/{id}', 'StepController@edit')->name('step.edit');
            Route::post('/admin/step/update', 'StepController@update')->name('step.update');
//            Route::post('/admin/step/delete', 'StepController@delete')->name('step.delete');
//            Route::post('/admin/step/pull_back', 'StepController@pullBack')->name('step.pull_back');

            /******************* 分销管理***************************************/
            // 优惠券列表
            Route::get('/agent/list','CouponController@index')->name('coupon.index');
            // 添加优惠券
            Route::get('/add/coupon','CouponController@add');
            // 操作添加
            Route::post('/add/costore','CouponController@store');
            // 编辑
            Route::get('/edit/coupon/{id}','CouponController@edit')->name('coupon.edit');
            // 操作编辑
            Route::post('/update/coupon','CouponController@update')->name('coupon.update');
            // 删除数据
            Route::post('/coupon/delete','CouponController@delete')->name('coupon.delete');


            // 申请列表
            Route::get('/master/list','MasterController@index')->name('master.index');
            // 添加优惠券
            Route::get('/add/master','MasterController@add');
            // 操作添加
            Route::post('/add/masters','MasterController@store');
            // 编辑
            Route::get('/edit/master/{id}','MasterController@edit')->name('master.edit');
            // 操作编辑
            Route::post('/update/master','MasterController@update')->name('master.update');
            // 删除数据
            Route::post('/master/delete','MasterController@delete')->name('master.delete');

            // 规则列表
            Route::get('/agentrole/list','AgentRoleController@index')->name('master.index');
            // 添加优惠券
            Route::get('/add/agentrole','AgentRoleController@add');
            // 操作添加
            Route::post('/add/agentroles','AgentRoleController@store');
            // 编辑
            Route::get('/edit/agentrole/{id}','AgentRoleController@edit')->name('master.edit');
            // 操作编辑
            Route::post('/update/agentrole','AgentRoleController@update')->name('master.update');
            // 删除数据
            Route::post('/agentrole/delete','AgentRoleController@delete')->name('master.delete');
            // 积分记录列表
            Route::get('/integral/list','IntegralLogController@index')->name('log.index');

            /*********************************************************************/
            // 文案列表
            Route::get('/article/list','ArticleController@index')->name('article.index');
            // 添加优惠券
            Route::get('/add/article','ArticleController@add')->name('article.add');
            // 操作添加
            Route::post('/add/articles','ArticleController@store')->name('article.adds');
            // 编辑
            Route::get('/edit/article/{id}','ArticleController@edit')->name('article.edit');
            // 操作编辑
            Route::post('/update/article','ArticleController@update')->name('article.update');
            // 删除数据
            Route::post('/article/delete','ArticleController@delete')->name('article.delete');

            // 门店列表
            Route::get('/shop/list','ShopController@index')->name('shop.index');
            // 添加门店
            Route::get('/add/shop','ShopController@add')->name('shop.add');
            // 操作添加
            Route::post('/add/shop','ShopController@store')->name('shop.adds');
            // 编辑
            Route::get('/edit/shop/{id}','ShopController@edit')->name('shop.edit');
            // 操作编辑
            Route::post('/update/shop','ShopController@update')->name('shop.update');
            // 删除数据
            Route::post('/shop/delete','ShopController@delete')->name('shop.delete');
            // 导入门店
            Route::get('/shop/import','ShopController@addImport')->name('shop.import');
            // 处理导入
            Route::post('/shop/imports','ShopController@storeImport')->name('shop.imports');
            // 会员打卡列表
            Route::get('/shop/user','ShopController@getUserShop')->name('usershop.index');
            // 门店账号
            Route::get('/account','ShopController@shopAccount')->name('account.add');
            // 添加门店
            Route::post('/save/account','ShopController@shopSave')->name('account.save');


            // 活动规则列表
            Route::get('/activity/list','ActivityRulesController@index')->name('activity.index');
            // 添加活动规则
            Route::get('/add/activity','ActivityRulesController@add')->name('activity.add');
            // 操作添加
            Route::post('/add/activity','ActivityRulesController@store')->name('activity.adds');
            // 编辑
            Route::get('/edit/activity/{id}','ActivityRulesController@edit')->name('activity.edit');
            // 操作编辑
            Route::post('/update/activity','ActivityRulesController@update')->name('activity.update');
            // 删除数据
            Route::post('/activity/delete','ActivityRulesController@delete')->name('activity.deletes');


            // Banner列表
            Route::get('/banner/list','BannerController@index')->name('banner.index');
            // 添加活动规则
            Route::get('/add/banner','BannerController@add')->name('banner.add');
            // 操作添加
            Route::post('/add/banner','BannerController@store')->name('banner.adds');
            // 编辑
            Route::get('/edit/banner/{id}','BannerController@edit')->name('banner.edit');
            // 操作编辑
            Route::post('/update/banner','BannerController@update')->name('banner.update');
            // 删除数据
            Route::post('/banner/delete','BannerController@delete')->name('banner.delete');



        });
    });
    Route::match(['get','post'],'/admin/admin/edit_head_portrait','AdminController@editHeadPortrait');
    Route::get('/admin/login','IndexController@login');
    Route::get('/admin/error','ErrorController@error');
    Route::get('/admin/login_out','IndexController@loginOut');
    Route::post('/admin/doLogin','IndexController@doLogin')->name('doLogin');
});

// 前台路由
Route::namespace('Home')->group(function () {
    Route::get('/member_check','UsersController@memberCheck'); // 1.周黑鸭手机号码查询是否注册了会员
    Route::get('/member_query','UsersController@memberQuery'); // 2.周黑鸭查询会员的手机号、openId、支付宝的userId、虚拟卡号
    Route::get('/crm_member_query','UsersController@crmMemberQuery'); // 3.周黑鸭查询周黑鸭Crm会员
    Route::post('/member_register','UsersController@memberRegister'); // 4.周黑鸭会员注册
    Route::get('/member_balance','UsersController@memberQueryWithBalance'); // 5.周黑鸭查询查询会员（返回余额积分）
    Route::get('/get_member_by_code','UsersController@getMemberByCode'); // 6.周黑鸭查询查询会员（返回余额积分）
    Route::get('/crm_order_list','UsersController@crmOrderList'); // crm会员订单列表
    Route::get('/send_sms','UsersController@sendSms'); // 发送短信验证码
    Route::get('/verify_sms','UsersController@verifySms'); // 验证短信验证码
    Route::get('/get_coupons','UsersController@getCoupons'); // 验证短信验证码

    //Route::any('/get_mobile','MiniProgramController@getMiniProgramMobile'); // 获取小程序手机号码
    Route::match(['get', 'post'], '/get_mobile', 'MiniProgramController@getMiniProgramMobile');
    Route::get('/test','MiniProgramController@test'); // 测试


    Route::post('/img_uploads','UploadsController@uploads_img'); // 上传图片
    //Route::get('/send_sms','SmsController@sendSms'); // 发送短信
    Route::get('/mall_list','MallController@mallList'); // 获取积分商品列表
    Route::get('/get_mall','MallController@getMallRow'); // 获取单个积分商品详情
    Route::post('/mall_order','MallController@mallOrder'); // 换取积分商品
    Route::post('/order_list','MallController@getUserOrder'); // 用户的积分订单
    Route::post('/order_qrcode','MallController@getOrderQrcode'); // 生成订单的核销码
    Route::post('/user_step','WeChatController@getUserStep'); // 获取微信用户的步数
    Route::get('/step','WeChatController@step'); // 获取微信用户的步数


    Route::post('/agent_apply','AgentController@agentApply'); // 成为导购员
    Route::get('/agent_info','AgentController@AgentInfo'); // 分销中心页
    Route::get('/agent_list','AgentController@AgentList'); // 我的荐客页


    Route::get('/active_test','UsersActiveController@activeTest'); // 查询活动
    Route::get('/active','UsersActiveController@active'); // 查询活动
    Route::post('/add_active','UsersActiveController@addActive'); // 参加活动

    /********************************************************* 门店相关********************/



    // 成为导购员
    Route::get('/set_agent','AgentController@setAgent');
    // 生成二维码
    Route::get('/set_qrcode','AgentController@getQrcode');
    // 获取推荐列表
    Route::post('/agent_list','AgentController@AgentList');
    // 绑定关系
    Route::post('/bind_agent','AgentController@userAgent');
    // 分销中心
    Route::post('/get_info','AgentController@getIndex');
    // 判断用户是否是导购员
    Route::post('/is_agent','AgentController@getStatus');
    // 获取收益明细
    Route::post('/get_money','AgentController@getUserMoney');
    // 设置地区
    Route::post('/set_area','AgentController@setArea');

    // 门店打卡
    Route::post('/shop_clock','ShopClockController@userClock');
    // 兑换积分
    Route::post('/get_clock','ShopClockController@isClock');
    // 门店打卡列表
    Route::post('/shoplist','ShopClockController@getShopList');
    // 门店打卡优惠券
    Route::post('/shop_coupon','ShopClockController@getCoupon');

    // 获取范围内的门店
    Route::post('/shop_list','ShopClockController@getShop');

    // 积分商城
    Route::post('/point_list','MallController@getAllList');
    // 积分兑换
    Route::post('/getmall','MallController@mallOrder');
    // 我的积分订单
    Route::post('/point_order','MallController@getUserOrder');
    // 订单核销二维码
    Route::post('/order_qrcode','MallController@getOrderQrcode');
    // 最新 和热门积分
    Route::post('/mall_list','MallController@mallList');
    // 门店核销二维码
    Route::post('/unqrcode','MallController@unsetQrcode');
    // 门店核销的页面
    Route::post('/unview','MallController@unView');
    // 门店登录
    Route::post('/shop_login','MallController@shopLogin');

    Route::post('/qrcode_img','MallController@getInfoImg');

    // 活动专属二维码
    Route::post('/qrcode_active','MallController@getActivies');




    // text专门测试的接口
    Route::post('/testss','UsersController@textSoap');

    Route::group(['middleware' => 'auth.jwt'], function () {

        Route::get('/my_active','UsersActiveController@activeList'); // 我参加活动的列表
        Route::get('/user_info','UsersController@userInfo'); // 会员个人信息接口
        Route::get('/bank_list','UsersController@bankList'); // 会员银行卡接口
        Route::post('/bind_bank','UsersController@bindBank'); // 会员绑定银行卡接口
        Route::post('/set_withdrawal_pwd','UsersController@setWithdrawalPassword'); // 会员绑定银行卡接口
        Route::post('/test','UsersController@text');

    });
});
